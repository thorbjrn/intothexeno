﻿using System;
using UnityEditor;
using UnityEngine;

public static class EditorGUIHelper
{
	public static string BasicTypeField(string serializedValue, string fieldName, Type fieldType)
	{
		if (fieldType == typeof(System.Int32))
		{
			int val = 0;
			try
			{
				val = int.Parse(serializedValue);
			}
			catch
			{
				val = 0;
			}
			val = EditorGUILayout.IntField(fieldName, val);
			serializedValue = val.ToString();

		}
		else if (fieldType == typeof(System.String))
		{

			serializedValue = EditorGUILayout.TextField(fieldName, serializedValue);
		}
		else if (fieldType == typeof(System.Boolean))
		{
			bool val = false;
			try
			{
				val = bool.Parse(serializedValue);
			}
			catch
			{
				val = false;
			}
			val = EditorGUILayout.Toggle(fieldName, val);
			serializedValue = val.ToString();
		}
		else if (fieldType == typeof(Vector2Int))
		{
			var val = Vector2Int.zero;
			try
			{
				val = new Vector2Int(
					int.Parse(serializedValue.Split('_')[0]),
					int.Parse(serializedValue.Split('_')[1])
				);
			}
			catch
			{
				val = Vector2Int.zero;
			}
			val = EditorGUILayout.Vector2IntField(fieldName, val);
			serializedValue = val.x + "_" + val.y;
			//Debug.Log("serializedValue: " + serializedValue);
		}

		return serializedValue;
	}
}