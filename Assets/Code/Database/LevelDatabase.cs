﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

namespace IntoTheXeno.Databases
{
	[CreateAssetMenu]
	public class LevelDatabase : ADatabase
	{
		[SerializeField] private List<LevelDefinition> levelDefinitions;

		public List<LevelDefinition> GetAllLevels(){
			return levelDefinitions;
		}

		public Level GetLevel(string levelId)
		{
			try
			{
				return levelDefinitions.First(x => x.id == levelId).level;
			}
			catch
			{
				Debug.LogError("No level for id: " + levelId);
				return null;
			}
		}
	}

	[Serializable]
	public class LevelDefinition
	{
		public string id;
		public Level level;
	}
}