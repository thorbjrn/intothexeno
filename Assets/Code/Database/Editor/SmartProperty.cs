﻿using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

public class SmartProperty<T>
{
	private readonly Dictionary<string, SerializedProperty> properties = new Dictionary<string, SerializedProperty>();
	private readonly SerializedProperty self;

	public bool IsExpanded
	{
		get { return self.isExpanded; }
		set { self.isExpanded = value; }
	}

	public string Name
	{
		get { return self.name; }
	}

	public SmartProperty(SerializedProperty property)
	{
		self = property;
		foreach (var field in typeof(T).GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
		{

			var prop = property.FindPropertyRelative(field.Name);
			//Debug.Log("field: " + field.Name + ", prop: " + prop.name);
			properties.Add(field.Name, prop);
		}
	}

	public SerializedProperty Get(string fieldName)
	{
		return properties[fieldName];
	}

	public static implicit operator SerializedProperty(SmartProperty<T> smartProperty)
	{
		return smartProperty.self;
	}
}