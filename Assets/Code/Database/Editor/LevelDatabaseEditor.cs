﻿using System;
using System.Linq.Expressions;
using IntoTheXeno.Databases;
using IntoTheXeno.Editor;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelDatabase))]
public class LevelDatabaseEditor : AGenericEditor<LevelDatabase>
{
	private SerializedProperty levelDefinitions
	{
		get { return serializedObject.FindProperty("levelDefinitions"); }
	}

	protected override void DrawInspectorGUI()
	{
		for (int i = 0; i < levelDefinitions.arraySize; i++)
		{
			DrawLevelDefinition(i);
		}

		if (GUILayout.Button("+"))
		{
			AddLevel();
		}
	}

	private void AddLevel()
	{
		levelDefinitions.arraySize++;
		var levelDef = new SmartProperty<LevelDefinition>(levelDefinitions.GetArrayElementAtIndex(levelDefinitions.arraySize - 1));
		var level = levelDef.Get("level").objectReferenceValue;
		if (level == null)
		{
			Debug.LogError("No level exists for defintiion!");
			return;
		}

		var newLevel = PrefabHelper.DuplicatePrefab((Component)level);
		levelDef.Get("level").objectReferenceValue = newLevel;
		levelDef.Get("id").stringValue = newLevel.name;

		LoadLevel(levelDef);
	}

	private void DeleteLevel(int index)
	{
		var levelDef = new SmartProperty<LevelDefinition>(levelDefinitions.GetArrayElementAtIndex(index));
		var level = levelDef.Get("level").objectReferenceValue;
		if (level != null) {
			PrefabHelper.DeletePrefab((Component)level);
		}
		levelDefinitions.DeleteArrayElementAtIndex(index);
	}

	private void DrawLevelDefinition(int index)
	{
		var levelDef = new SmartProperty<LevelDefinition>(levelDefinitions.GetArrayElementAtIndex(index));
		levelDef.IsExpanded = EditorGUILayout.Foldout(levelDef.IsExpanded, levelDef.Name, true);
		if (levelDef.IsExpanded)
		{
			int indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel += 2;
			//EditorGUILayout.BeginVertical();
			if (GUI.Button(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "Load"))
			{
				LoadLevel(levelDef);
			}

			EditorGUILayout.PropertyField(levelDef.Get("id"));
			EditorGUILayout.PropertyField(levelDef.Get("level"));
			if (GUI.Button(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "-"))
			{
				DeleteLevel(index);
			}
			//EditorGUILayout.EndVertical();
			EditorGUI.indentLevel = indent;
		}

	}

	private void LoadLevel(SmartProperty<LevelDefinition> levelDef)
	{
		var level = levelDef.Get("level").objectReferenceValue;
		if (level == null)
		{
			Debug.LogError("No level exists for defintiion!");
			return;
		}

		var currentLevel = FindObjectOfType<Level>();
		if (currentLevel != null)
		{
			DestroyImmediate(currentLevel.gameObject);
		}

		PrefabUtility.InstantiatePrefab(level);
	}
}
