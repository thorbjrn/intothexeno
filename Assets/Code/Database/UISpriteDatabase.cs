﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace IntoTheXeno.Databases
{
	[CreateAssetMenu]
	public class UISpriteDatabase : ADatabase
	{
		[SerializeField] private List<NamedSprite> actionSprites;
	
		public Sprite GetActionSprite(string actionId){
			try
			{
				return actionSprites.First(x => x.id == actionId).sprite;
			}
			catch
			{
				Debug.LogError("No sprite for action: " + actionId);
				return null;
			}
		}
	}

	[Serializable]
	public class NamedSprite{
		public Sprite sprite;
		public string id;
	}
}