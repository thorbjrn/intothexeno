﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace IntoTheXeno.Databases
{
	[CreateAssetMenu]
	public class UnitDatabase : ADatabase
	{
		[SerializeField] ActorPiece piecePrefab;
		[SerializeField] UnitDefinition[] _unitDefinitions;

		public Unit CreateUnit(string unitType)
		{
			var unitDefintion = _unitDefinitions.First(x => x.unitType == unitType);

			var unitPiece = Instantiate(piecePrefab);
			unitPiece.gameObject.name = unitType;
			unitPiece.SetAnimController(unitDefintion.animController);

			var unit = new Unit { ActorPiece = unitPiece };
			unit.Data = unitDefintion.unitData;

			return unit;
		}
	}
}