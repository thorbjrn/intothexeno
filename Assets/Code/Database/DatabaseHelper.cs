﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using IntoTheXeno.Databases;
using UnityEditor;
using UnityEngine;

public class DatabaseHelper : MonoBehaviour
{
	private static DatabaseHelper instance;

	[SerializeField] private ADatabase[] databases;

	public ADatabase[] Databases
	{
		get { return databases; }
	}
	
	[InitializeOnLoadMethod]
	private static void Initialize()
	{
//		Debug.Log("DatabaseHelper InitializeOnLoad");

		if (instance == null)
		{
			instance = FindObjectOfType<DatabaseHelper>();

			if (instance == null)
			{
				instance = new GameObject("DatabaseHelper").AddComponent<DatabaseHelper>();
				instance.gameObject.hideFlags = HideFlags.NotEditable;
			}
		}

		instance.UpdateDatabaseReferences();
	}

	private void UpdateDatabaseReferences()
	{
		databases = Resources.FindObjectsOfTypeAll<ADatabase>();
//		Debug.Log("UpdateDatabaseReferences - databases: " + string.Join(", ", databases.Select(x => x.name).ToArray()));
	}
}