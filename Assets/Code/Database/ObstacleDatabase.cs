﻿using UnityEngine;
using System.Collections;
using IntoTheXeno.Databases;
using System;
using System.Linq;

namespace IntoTheXeno.Databases
{
	[CreateAssetMenu]
	public class ObstacleDatabase : ADatabase
	{
		[SerializeField] ActorPiece piecePrefab;
		[SerializeField] ObstacleDefinition[] _obstacleDefinitions;

		public Obstacle CreateObstacle(string unitType, bool isSimulated)
		{
			var obstacleDefinition = _obstacleDefinitions.First(x => x.obstacleType == unitType);

			IActorPiece actorPiece = null;
			if (isSimulated){
				actorPiece = new FakeActorPiece();
			}else{
				actorPiece = Instantiate(piecePrefab);
				((ActorPiece)actorPiece).gameObject.name = unitType;
			}

			var obstacle = new Obstacle { ActorPiece = actorPiece };
			obstacle.Spawn(obstacleDefinition.health);

			return obstacle;
		}
	}

	[Serializable]
	public class ObstacleDefinition
	{
		public string obstacleType;
		public int health;
		public Sprite sprite;
	}

}