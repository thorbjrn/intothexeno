using UnityEngine;
using System.Collections;
using IntoTheXeno.Databases;
using System;
using System.Linq;

namespace IntoTheXeno.Databases
{
	[CreateAssetMenu]
	public class RulesDatabase : ADatabase
	{
		[SerializeField] public LevelType testLevelType;
		[SerializeField] public UnitOrderType unitOrderType;

	}
	
	public enum LevelType { SIDE_2D, TOPDOWN_2D }
	public enum UnitOrderType { ALTERNATING_TEAMS, INITIATIVE}
	

}