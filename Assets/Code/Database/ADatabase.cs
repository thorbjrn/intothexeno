﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;
using System;
using System.Collections.Generic;

namespace IntoTheXeno.Databases
{

	public abstract class ADatabase : ScriptableObject 
	{
		private static readonly List<ADatabase> instances = new List<ADatabase>();

		public static T Get<T>() where T : ADatabase
		{
			var instance = instances.FirstOrDefault(x => x.GetType() == typeof(T));
			if (instance == null)
			{
				if (instance == null)
				{
					var databases = FindObjectOfType<DatabaseHelper>().Databases;
					
					instance = databases.FirstOrDefault(x => x.GetType() == typeof(T));
				}

				if (instance != null){
					instances.Add(instance);
				}
			}
			try {
				instance = (T)instance;
			}catch(Exception e){
				Debug.LogError(e + "\ntypeof(T):" + typeof(T).ToString() + " instance: " + instance + " , instance.GetType: "+ (instance != null ? instance.GetType().ToString() : "NULL"));
			}
			return (T)instance;
		}

	}
}

