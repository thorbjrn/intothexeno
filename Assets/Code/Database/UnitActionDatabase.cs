﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace IntoTheXeno.Databases
{
	[CreateAssetMenu]
	public class UnitActionDatabase : ADatabase
	{
		[SerializeField] UnitActionDefinition[] _unitActionDefinitions;

		public UnitActionDefinition GetUnitActionDefinition(string actionId)
		{
			return _unitActionDefinitions.FirstOrDefault(x => x.actionId == actionId);
		}

		internal List<UnitActionDefinition> GetUnitActionDefinitions(string[] actionIds)
		{
			var unitActionsDefinitions = new List<UnitActionDefinition>();
			foreach (var actionId in actionIds)
			{
				var actionDefinition = GetUnitActionDefinition(actionId);
				unitActionsDefinitions.Add(actionDefinition);
			}
			return unitActionsDefinitions;
		}
	}


#if UNITY_EDITOR
	[CustomEditor(typeof(UnitActionDatabase))]
	public class UnitActionDatabaseEditor : Editor
	{

		static string[] targetTypes;
		static string[] effectTypes;

		Texture2D backgroundTex;

		Texture2D BackgroundTex
		{
			get
			{
				backgroundTex = new Texture2D(4, 4);
				var pixels = new Color32[backgroundTex.width * backgroundTex.height];
				for (int i = 0; i < pixels.Length; i++) pixels[i] = Color.red;
				backgroundTex.SetPixels32(pixels);
				backgroundTex.Apply();
				Debug.Log("pixels: " + string.Join(", ", backgroundTex.GetPixels32().Select(x => x.ToString()).ToArray()));
				//}
				return backgroundTex;
			}
		}

		Color BACKGROUND_COLOR = new Color(0.5f, 0.5f, 0.5f, 1f);

		static UnitActionDatabaseEditor()
		{
			targetTypes = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(s => s.GetTypes())
				.Where(p => typeof(ITargetType).IsAssignableFrom(p) && !p.IsInterface)
				.Select(x => x.Name).ToArray();
			effectTypes = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(s => s.GetTypes())
				.Where(p => typeof(IEffectType).IsAssignableFrom(p) && !p.IsInterface)
				.Select(x => x.Name).ToArray();

		}


		public override void OnInspectorGUI()
		{
			// Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
			serializedObject.Update();

			var actionsProp = serializedObject.FindProperty("_unitActionDefinitions");
			int deleteArrayIdx = -1;
			for (int i = 0; i < actionsProp.arraySize; i++)
			{
				var actionProp = actionsProp.GetArrayElementAtIndex(i);

				var actionIdProp = actionProp.FindPropertyRelative("actionId");
				var targetsProp = actionProp.FindPropertyRelative("targets");
				var effectsProp = actionProp.FindPropertyRelative("effects");

				var rect = EditorGUILayout.BeginVertical();
				EditorGUI.DrawRect(rect, BACKGROUND_COLOR);
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.PropertyField(actionIdProp);
				if (GUILayout.Button("-"))
				{
					deleteArrayIdx = i;
				}
				EditorGUILayout.EndHorizontal();

				SerializedArray(targetsProp, targetTypes);

				SerializedArray(effectsProp, effectTypes);
				EditorGUILayout.EndVertical();

			}

			if (deleteArrayIdx != -1)
			{
				actionsProp.DeleteArrayElementAtIndex(deleteArrayIdx);
			}
			if (GUILayout.Button("+"))
			{
				actionsProp.arraySize++;
			}

			// Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
			serializedObject.ApplyModifiedProperties();
		}

		private void SerializedArray(SerializedProperty arrayProp, string[] types)
		{
			EditorGUI.indentLevel = 2;

			arrayProp.isExpanded = EditorGUILayout.Foldout(arrayProp.isExpanded, arrayProp.name, true);
			if (arrayProp.isExpanded)
			{
				int deleteArrayIdx = -1;
				for (int j = 0; j < arrayProp.arraySize; j++)
				{
					var prop = arrayProp.GetArrayElementAtIndex(j);
					bool shouldDelete = SerializedElement(prop.FindPropertyRelative("serializedType"), prop.FindPropertyRelative("serializedFields"), types);
					if (shouldDelete)
					{
						deleteArrayIdx = j;
					}
				}
				if (deleteArrayIdx != -1)
				{
					arrayProp.DeleteArrayElementAtIndex(deleteArrayIdx);
				}

				if (GUI.Button(EditorGUI.IndentedRect(EditorGUILayout.GetControlRect()), "+"))
				{
					arrayProp.arraySize++;
				}
			}


			EditorGUI.indentLevel = 0;
		}

		private bool SerializedElement(SerializedProperty typeProp, SerializedProperty fieldsProp, string[] types)
		{
			int index;
			try
			{
				index = types
					.Select((v, idx) => new { Name = v, Index = idx })
					.First(x => x.Name == typeProp.stringValue)
					.Index;
			}
			catch
			{
				index = 0;
			}

			EditorGUILayout.BeginHorizontal();
			typeProp.stringValue = types[EditorGUILayout.Popup(index, types)];
			bool shouldDelete = GUILayout.Button("-");
			EditorGUILayout.EndHorizontal();

			var targetTypeFields = Type.GetType(typeProp.stringValue).GetFields();
			var fields = fieldsProp.stringValue.Split(',');
			string serializedFields = "";

			foreach (var fieldInfo in targetTypeFields)
			{
				var serializedValue = fields.FirstOrDefault(x => x.Contains(fieldInfo.Name));
				try
				{
					serializedValue = serializedValue.Split(':')[1];
				}
				catch
				{
					serializedValue = "";
				}

				serializedValue = EditorGUIHelper.BasicTypeField(serializedValue, fieldInfo.Name, fieldInfo.FieldType);

				serializedFields += fieldInfo.Name + ":" + serializedValue + ",";
			}

			fieldsProp.stringValue = serializedFields;

			return shouldDelete;
		}

	}

#endif
}