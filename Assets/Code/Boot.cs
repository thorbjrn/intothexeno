﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntoTheXeno.Databases;
using UnityEngine;

namespace IntoTheXeno
{
	public class Boot : MonoBehaviour
	{
		[SerializeField] List<RosterUnit> TEST_HeroUnits;
		public static List<RosterUnit> TEST_Heroes { get { return instance.TEST_HeroUnits; } }
		[SerializeField] List<RosterUnit> TEST_EnemyUnits;
		public static List<RosterUnit> TEST_Enemies { get { return instance.TEST_EnemyUnits; } }

		[SerializeField] BootKey bootKey;
		[SerializeField] string testLevelId;
		[SerializeField] BootInput[] testInputs;


		private static Boot instance;
		private void Awake()
		{
			instance = this;
		}

		void Start()
		{
			switch (bootKey)
			{
				case BootKey.NORMAL:
					StartMenuFlow();
					break;
				case BootKey.TEST_BATTLE:
					StartTestBattle();
					break;
			}

		}

		private void StartMenuFlow(){
			var menuFlow = new MainFlow();
			StartCoroutine(menuFlow.Flow());
		}

		private void InitiativeTest(){ //TODO Move to test class?
			var units = new List<TestUnit>{
				new TestUnit {Id = "A", Initiative = 16, TurnsSinceLastMove = 1},
				new TestUnit {Id = "B", Initiative = 12, TurnsSinceLastMove = 1},
				new TestUnit {Id = "C", Initiative = 6, TurnsSinceLastMove = 1},
			};

			string output = "";

			//The initiative is the number of actions that a unit gets over the course of 10 turns
			//Unit gets a new action every (10/X) turns, where X is the initiative.
			//A: 10/10 	= 1
			//B: 10/7 	= 1.4285714286		
			//C: 10/3 	= 3.3333333333 

			foreach (var unit in units)
			{
				unit.ActionsLeft = 10f / unit.Initiative;
				Debug.Log(unit.Id + " starts with TurnsLeft: " + unit.ActionsLeft);
			}

			int turns = 100;
			for (int i = 0; i < turns; i++)
			{
				Debug.Log("### Turn: " + (i + 1) + " ### - turnsLeft: " + string.Join(", ", units.Select(x => x.Id + ":" + x.ActionsLeft).ToArray()));

				foreach (var unit in units)
				{
					unit.ActionsLeft -= 1f;
				}

				int derpCounter = 0;
				while (derpCounter++ < 1000 && units.Any(x => x.ActionsLeft <= 1f))
				{
					foreach (var unit in units.OrderBy(x => x.ActionsLeft))
					{
						if (unit.ActionsLeft <= 1f)
						{
							unit.ActionsLeft += 10f / unit.Initiative;
							unit.TurnsTaken++;
							output += unit.Id;
							Debug.Log(unit.Id + " took action, now has TurnsLeft: " + unit.ActionsLeft);
						}
					}
				}

			}


			foreach (var item in units)
			{
				Debug.Log(item.Id + " turns taken: " + item.TurnsTaken);
			}

			Debug.Log(output);
		}

		private void StartTestBattle()
		{
			Menus.CloseAll();

			var heroRoster = new Roster { units = TEST_HeroUnits };
			var enemyRoster = new Roster { units = TEST_EnemyUnits };
			var heroFactionInput = testInputs[0] == BootInput.HUMAN ? (FactionInput)new HumanInput() : new BotInput();
			var enemyFactionInput = testInputs[1] == BootInput.HUMAN ? (FactionInput)new HumanInput() : new BotInput();

			heroFactionInput.factionId = 0;
			enemyFactionInput.factionId = 1;

			var battleFlow = new BattleFlow(testLevelId, heroFactionInput, enemyFactionInput, heroRoster, enemyRoster);
			StartCoroutine(battleFlow.Flow());
		}

		[Serializable] public enum BootKey { NORMAL, TEST_BATTLE }
		[Serializable] public enum BootInput { HUMAN, BOT }


		private class TestUnit{
			public string Id;
			public float Initiative;
			public float ActionsLeft;
			public float TurnsSinceLastMove;
			public int TurnsTaken;

			public float GetWeight(){
				return Initiative + TurnsSinceLastMove;
			}
		}
	}
}