﻿using System.Collections;
using System.Collections.Generic;
using IntoTheXeno.Databases;
using UnityEngine;

public class LevelSelectMenu : AMenu
{
	[SerializeField] LevelSelectItem levelSelectItemTemplate;

	protected override void Awake()
	{
		base.Awake();

		levelSelectItemTemplate.gameObject.SetActive(false);

		var levels = ADatabase.Get<LevelDatabase>().GetAllLevels();
		foreach (var level in levels)
		{
			var levelSelectItem = Instantiate(levelSelectItemTemplate, levelSelectItemTemplate.transform.parent);
			levelSelectItem.Init(level.level.name);
			levelSelectItem.Button.onClick.AddListener(() => ClickedPlay(level.id));
		}
	}

	public void ClickedPlay(string levelId)
	{
		Debug.Log("ClickedPlay - levelId: " + levelId);

		Menus.OnStartLevel(levelId);
	}

	public void ClickedBack(){
		Menus.CloseTopMenu();
	}
}
