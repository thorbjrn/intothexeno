﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : AMenu
{
	public void ClickedStartGame()
	{
		Close();
	}

	public void ClickedLevelSelect()
	{
		Menus.Show<LevelSelectMenu>();
	}

	public void ClickedExit()
	{
		Application.Quit();
	}
}
