﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public abstract class AMenu : MonoBehaviour
{
	protected bool showing;

	CanvasGroup canvasGroup;

	protected virtual void Awake()
	{
		canvasGroup = GetComponent<CanvasGroup>();
	}


	public virtual void Show()
	{
		showing = true;
		gameObject.SetActive(true);
		SetVisible(true);
	}

	public virtual void Close()
	{
		showing = false;
		SetVisible(false);
	}

	public virtual IEnumerator WaitForClose()
	{
		while (showing) { yield return null; }
	}

	internal void SetVisible(bool isVisible)
	{
		canvasGroup.alpha = isVisible ? 1f : 0f;
		canvasGroup.blocksRaycasts = isVisible;
		canvasGroup.interactable = isVisible;
	}
}
