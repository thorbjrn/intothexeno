﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectItem : MonoBehaviour
{
	[SerializeField] private Text titleText;

	public Button Button {
		get {
			return GetComponentInChildren<Button>();
		}
	}

	public void Init(string title){
		gameObject.SetActive(true);
		titleText.text = title;
	}
}
