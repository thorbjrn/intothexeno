﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IntoTheXeno.UI
{
	public class HealthBar : MonoBehaviour
	{
		[SerializeField] private Image healthChunkTemplate;
		[SerializeField] private HorizontalLayoutGroup layoutGroup;

		private List<Image> healthChunks = new List<Image>();

		AActor actor;

		void Update()
		{
			if (actor == null) return;
			var actorPiece = actor.ActorPiece as ActorPiece;
			if (actorPiece == null) return;
			this.transform.position = actorPiece.transform.position;
		}

		public void Inititalize(AActor actor)
		{
			foreach (var item in healthChunks)
			{
				Destroy(item.gameObject);
			}
			healthChunks.Clear();

			this.actor = actor;
			for (int i = 0; i < actor.Health; i++)
			{
				var healthChunk = Instantiate(healthChunkTemplate, healthChunkTemplate.transform.parent);
				healthChunk.gameObject.SetActive(true);
				//Debug.Log("actor.FactionId: " + actor.FactionId);
				var color = actor.FactionId == 0 ? Color.green : Color.red;

				healthChunk.GetComponent<Image>().color = color;
				healthChunks.Add(healthChunk);
			}
		}

		public void SetCurrentHealth(int health){
			if (health < 0 || health > healthChunks.Count){
				Debug.LogError("Invalid health! - health: " + health);
				return;
			}

			for (int i = 0; i < healthChunks.Count; i++)
			{
				healthChunks[i].enabled = health > i;
			}
		}
	}
}

