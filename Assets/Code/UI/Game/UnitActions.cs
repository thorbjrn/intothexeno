﻿using System;
using System.Collections;
using System.Collections.Generic;
using IntoTheXeno.Databases;
using UnityEngine;
using UnityEngine.UI;

namespace IntoTheXeno.UI
{
	public class UnitActions : MonoBehaviour
	{
		[SerializeField] private Image[] images;

		private void Awake()
		{
			EventManager.OnShowUnitActions += OnShowUnitActions;
		}

		private void OnShowUnitActions(List<UnitActionDefinition> unitActionDefinitions, bool[] usageState)
		{
			for (int i = 0; i < images.Length; i++)
			{
				var image = images[i];
				bool isAvailable = i < unitActionDefinitions.Count;
				bool isUsing = usageState[i];
				image.gameObject.SetActive(isAvailable);
				if (isAvailable)
				{
					var unitActionDefinition = unitActionDefinitions[i];
					image.sprite = ADatabase.Get<UISpriteDatabase>().GetActionSprite(unitActionDefinition.actionId);
					image.color = isUsing ? Color.gray : Color.white;
				}


			}
		}
	}

}