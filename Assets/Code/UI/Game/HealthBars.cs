﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IntoTheXeno.UI
{
	public class HealthBars : MonoBehaviour
	{
		[SerializeField] TemplatePool healthBarPool;

		private Dictionary<AActor, HealthBar> healthBars = new Dictionary<AActor, HealthBar>();

		private void Awake()
		{
			EventManager.OnUpdateActorHealth += OnUpdateActorHealth;
			EventManager.OnActorDead += OnActorDead;
		}

		private void OnActorDead(AActor actor)
		{
//			Debug.Log("OnActorDead - ActorId: " + actor.ActorId + ", healthBars: " + healthBars[actor].GetComponentsInChildren<UnityEngine.UI.Image>().Length);

			var healthBar = healthBars[actor];
			healthBarPool.ReturnInstance(healthBar.gameObject);
			healthBars.Remove(actor);
		}

		private void OnUpdateActorHealth(AActor actor)
		{
			if (!actor.IsAlive) return;

			//Debug.Log("OnUpdateActorHealth - ActorId: " + actor.ActorId + ", health: " + actor.Health);
			HealthBar healthBar = null;
			if (!healthBars.ContainsKey(actor)){
				healthBar = healthBarPool.GetInstance<HealthBar>();
				healthBar.Inititalize(actor);
				healthBars.Add(actor, healthBar);
			}else{
				healthBar = healthBars[actor];
			}

			healthBar.SetCurrentHealth(actor.Health);
		}
	}

}
