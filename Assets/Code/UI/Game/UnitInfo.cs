﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IntoTheXeno.UI
{
	public class UnitInfo : MonoBehaviour
	{
		[SerializeField] private Text nameText;
		[SerializeField] private Text healthText;

		private void Awake()
		{
			EventManager.OnSelectedUnit += OnSelectedUnit;
		}

		private void OnSelectedUnit(Unit unit)
		{
			string name = ""; ;
			string health = ""; 
			try {
				name = "TODO Get name";
				health = "HP: " + unit.Health.ToString();
			} catch{}
			nameText.text = name;
			healthText.text = health;
		}
	}
}