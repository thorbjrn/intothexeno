﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MovementArrows : MonoBehaviour
{
	[SerializeField] List<ArrowPieceDefinition> arrowPieceDefinitions;
	[SerializeField] TemplatePool arrowPiecePool;

	private List<Image> arrowPieces = new List<Image>();

	private void Awake()
	{
		EventManager.OnShowPath += OnShowPath;
	}

	private void OnShowPath(Path path)
	{
		DrawArrow(path);
	}

	[ContextMenu("TestArrow")]
	public void TestArrow(){
		var path = new Path
		{
			positions = new List<Vector2Int>
			{
				new Vector2Int(2, 0),
				new Vector2Int(1, 0),
				new Vector2Int(1, 1),
				new Vector2Int(1, 2),
				new Vector2Int(0, 2),
				new Vector2Int(0, 3),
				new Vector2Int(1, 3),
				new Vector2Int(1, 4),
				new Vector2Int(0, 4),
				new Vector2Int(-1, 4),
				new Vector2Int(-2, 4),
				new Vector2Int(-2, 3),
				new Vector2Int(-3, 3),
				new Vector2Int(-3, 4),
				new Vector2Int(-3, 5),
				new Vector2Int(-3, 6),
				new Vector2Int(-2, 6),
				new Vector2Int(-2, 5),
			}
		};
		DrawArrow(path);
	}

	public void DrawArrow(Path path){
		//Debug.Log("DrawArrow - path: " + string.Join(", ", path.positions.Select(x => x.ToString()).ToArray()));

		foreach (var piece in arrowPieces)
		{
			arrowPiecePool.ReturnInstance(piece.gameObject);
		}
		arrowPieces.Clear();

		if (path.positions.Count < 2){
			//Debug.LogError("Too short arrow!");
			return;
		}

		for (int i = 0; i < path.positions.Count; i++)
		{
			var pos = path.positions[i];

			if (i == 0){
				DrawArrowStart(pos, path.positions[i + 1]);
			}else if (i == path.positions.Count - 1){
				DrawArrowEnd(path.positions[i - 1], pos);
			}else{
				DrawArrowLine(path.positions[i - 1], pos, path.positions[i + 1]);
			}
		}
	}

	private void DrawArrowStart(Vector2Int pos, Vector2Int nextPos)
	{
		var arrowPiece = GetArrowPiece(ArrowType.START, pos);
		var dir = nextPos - pos;
		arrowPiece.transform.right = new Vector2(dir.x, dir.y);
	}

	private void DrawArrowEnd(Vector2Int prevPos, Vector2Int pos)
	{
		var arrowPiece = GetArrowPiece(ArrowType.HEAD, pos);
		var dir = pos - prevPos;
		arrowPiece.transform.right = new Vector2(dir.x, dir.y);
	}

	private void DrawArrowLine(Vector2Int prevPos, Vector2Int pos, Vector2Int nextPos){
		bool isStraight = prevPos.x == nextPos.x || prevPos.y == nextPos.y;
		bool isCurvingLeft =
			prevPos.x + 1 == pos.x && pos.y - 1 == nextPos.y ||
			prevPos.x - 1 == pos.x && pos.y + 1 == nextPos.y ||
			prevPos.y + 1 == pos.y && pos.x + 1 == nextPos.x ||
			prevPos.y - 1 == pos.y && pos.x - 1 == nextPos.x;
		var pieceType = isStraight ? ArrowType.LINE : (isCurvingLeft ? ArrowType.CURVE_LEFT : ArrowType.CURVE_RIGHT);
		var arrowPiece = GetArrowPiece(pieceType, pos);
		var dir = pos - prevPos;
		arrowPiece.transform.right = new Vector2(dir.x, dir.y);
		if (pieceType != ArrowType.LINE){

			//var rot = Quaternion.FromToRotation(Vector3.right, new Vector3(dir.x, dir.y, 0f));
			if (Math.Abs(arrowPiece.transform.eulerAngles.y) > 0.00001)
			{
				arrowPiece.transform.eulerAngles = new Vector3(0, 0, arrowPiece.transform.eulerAngles.y);
			}

			//arrowPiece.transform.rotation = rot;
            //Debug.Log("isCurvingLeft: " + isCurvingLeft + ", path: " + prevPos + "->" + pos + "->" + nextPos + ", dir: " + dir + ", rot: "+ arrowPiece.transform.eulerAngles);

		}
	}

	private Image GetArrowPiece(ArrowType type, Vector2Int pos){
		//var pieceInstance = Instantiate(arrowPieceTemplate, arrowPieceTemplate.transform.parent);
		var pieceInstance = arrowPiecePool.GetInstance<Image>();

		var pieceDef = arrowPieceDefinitions.FirstOrDefault(x => x.arrowType == type);
		pieceInstance.transform.position = pos.ToVector3();
		pieceInstance.sprite = pieceDef.sprite;

		arrowPieces.Add(pieceInstance);

		return pieceInstance;
	}
}

[Serializable]
public class ArrowPieceDefinition{
	public ArrowType arrowType;
	public Sprite sprite;
}

[Serializable]
public enum ArrowType{
	START,
	LINE,
	CURVE_RIGHT,
	CURVE_LEFT,
	HEAD
}