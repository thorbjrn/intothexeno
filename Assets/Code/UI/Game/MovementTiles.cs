﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IntoTheXeno.UI
{
	public class MovementTiles : MonoBehaviour
	{
		[SerializeField] TemplatePool tilePool;

		private List<GameObject> tiles = new List<GameObject>();

		private void Awake()
		{
			EventManager.OnShowMovementPositions += OnShowMovementPositions;
		}

		private void OnShowMovementPositions(List<Vector2Int> positions)
		{
			foreach (var tile in tiles)
			{
				tilePool.ReturnInstance(tile);
			}
			tiles.Clear();

			foreach (var pos in positions)
			{
				var tile = tilePool.GetInstance();
				tile.transform.position = GetWorldCanvasPosition(pos);
				tiles.Add(tile);
			}
		}

		private Vector2 GetWorldCanvasPosition(Vector2Int vector2Int){
			return new Vector2(vector2Int.x + 0.5f, vector2Int.y + 0.5f);
		}
	}
}