﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoundOrderNumbers : MonoBehaviour
{
	[SerializeField] TemplatePool roundOrderNumbersPool;

	private List<Text> roundOrderNumbers = new List<Text>();

	private void Awake()
	{
		EventManager.OnShowRoundOrder += OnShowRoundOrder;
	}

	private void OnShowRoundOrder(bool show, LevelController levelController)
	{
		foreach (var number in roundOrderNumbers)
		{
			roundOrderNumbersPool.ReturnInstance(number.gameObject);
		}
		roundOrderNumbers.Clear();

		if (show){
			//foreach (var queuedUnit in levelState.QueuedUnits)
			//{

			//}
		}
	}
}
