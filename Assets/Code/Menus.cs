﻿using UnityEngine;
using System.Collections;
using IntoTheXeno.Databases;
using System;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(Canvas))]
public class Menus : MonoBehaviour
{
	static Menus instance;

	Stack<AMenu> menuStack = new Stack<AMenu>();

	public static Action<string> OnStartLevel;
	//public static Action OnOpenMenu;

	[SerializeField] AMenu[] menus;
	[SerializeField] RectTransform menuBackground;

	Canvas canvas;

	void Awake()
	{
		canvas = GetComponent<Canvas>();
		instance = this;
	}

	public static AMenu Show<T>() where T : AMenu
	{
		//Debug.Log("ShowMenu - type: " + typeof(T) + ", menuStack.Count: " + instance.menuStack.Count);

		var menu = instance.GetMenu<T>();
		instance.ShowMenu(menu);

		return menu;
	}

	public static void CloseAll()
	{
		foreach (var menu in instance.menuStack)
		{
			menu.Close();
		}
		instance.menuStack.Clear();

		instance.canvas.enabled = false;
	}

	public static void CloseTopMenu()
	{
		var menu = instance.menuStack.Pop();
		menu.Close();

		if (instance.menuStack.Count > 0)
		{
			var nextMenu = instance.menuStack.Peek();
			nextMenu.Show();
		}else{
			instance.canvas.enabled = false;
		}
	}

	private void ShowMenu(AMenu menu)
	{
		foreach (var m in menuStack){
			m.SetVisible(false);
		}

		menu.Show();

		instance.canvas.enabled = true;

		menuStack.Push(menu);
	}

	private T GetMenu<T>() where T : AMenu
	{
		//Debug.Log("GetMenu of type: " + typeof(T));
		var menu = menus.FirstOrDefault(x => x is T);
		if (menu == null) Debug.LogError("menu of type " + typeof(T) + " does not exists");
		return menu as T;
	}
}
