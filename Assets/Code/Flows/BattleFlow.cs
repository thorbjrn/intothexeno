﻿using IntoTheXeno.Databases;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

public class BattleFlow
{
	readonly LevelController levelController;

	public BattleFlow(string levelId, FactionInput faction1, FactionInput faction2, Roster roster1, Roster roster2)
	{
		var currentLevel = Object.FindObjectOfType<Level>();
		if (currentLevel) Object.Destroy(currentLevel.gameObject);

		var levelPrefab = ADatabase.Get<LevelDatabase>().GetLevel(levelId);
		var level = Object.Instantiate(levelPrefab);
		
		var levelState = ADatabase.Get<RulesDatabase>().testLevelType == LevelType.SIDE_2D ? (ILevelState)new Side2DLevelState() : new TopDown2DLevelState();
		levelController = new LevelController(levelState, level, faction1, faction2, roster1, roster2);

		faction1.LevelController = levelController;
		faction2.LevelController = levelController;
	}

	public IEnumerator Flow()
	{
		var unitOrderType = ADatabase.Get<RulesDatabase>().unitOrderType;
		
		//Loop through turns
		while (true)
		{
			switch (unitOrderType)
			{
				case UnitOrderType.INITIATIVE:
					yield return InitiativeFlow();
					break;
				case UnitOrderType.ALTERNATING_TEAMS:
					yield return AlternatingTeamsFlow();
					break;
			}
		}
	}

	private IEnumerator AlternatingTeamsFlow()
	{
		while (true)
		{
			//Go to next round
			levelController.RoundNumber++;
			
			Debug.Log("Starting round " + levelController.RoundNumber);
			
			//All enemy units take moves, and plans attack
			foreach (var unit in levelController.GetUnitsOfFaction(1))
			{
				yield return BotUnitMovementFlow(unit);
			}

			//All player units take turn
			foreach (var unit in levelController.GetUnitsOfFaction(0))
			{
				yield return UnitFlow(unit);				
			}
			
			//All enemy units attack
			foreach (var unit in levelController.GetUnitsOfFaction(1))
			{
				yield return BotUnitAttackFlow(unit);
			}
		}
	}

	private IEnumerator InitiativeFlow()
	{
		while (true)
		{
			var unit = levelController.GetNextUnitByInitiative();
			yield return UnitFlow(unit);
		}
	}

	private IEnumerator UnitFlow(Unit unit)
	{
		if (unit.Health <= 0) yield break;
			
		unit.StartTurn();

		var inputTypes = new[] {InputType.MOVE, InputType.ACTION, InputType.END_TURN};

		var unitInputWork = new Work<UnitInput>();
		var possiblePaths = levelController.GetPossiblePaths(unit);
		EventManager.OnSelectedUnit(unit);
		EventManager.OnShowUnitActions(unit.UnitActionsDefinitions, new[] { false, false });
		EventManager.OnShowMovementPositions(possiblePaths.Keys.ToList());

		while (unit.ActorPiece.IsActive)
		{
			yield return unitInputWork.Run(unit.FactionInput.GetUnitInput(unit, inputTypes));

			EventManager.OnShowMovementPositions(new List<Vector2Int>());
			EventManager.OnShowPath(new Path());

			yield return levelController.PerformUnitInput(unit, unitInputWork.result);
		}

		EventManager.OnShowUnitActions(unit.UnitActionsDefinitions, new[] { false, false });
		EventManager.OnShowMovementPositions(new List<Vector2Int>());
		EventManager.OnUpdateActorHealth(unit);
		EventManager.OnShowPath(new Path());
	}

	private IEnumerator BotUnitMovementFlow(Unit unit)
	{
		if (unit.Health <= 0) yield break;

		unit.StartTurn();
		
		var unitInputWork = new Work<UnitInput>();

		yield return unitInputWork.Run(
			unit.FactionInput.GetUnitInput(unit, new[] {InputType.MOVE})
		);
		
		yield return levelController.PerformUnitInput(unit, unitInputWork.result);

		yield return unitInputWork.Run(
			unit.FactionInput.GetUnitInput(unit, new[] {InputType.ACTION})
		);

		unit.QueuedInput = unitInputWork.result;
	}
	
	private IEnumerator BotUnitAttackFlow(Unit unit)
	{
		if (unit.Health <= 0) yield break;
		
		yield return levelController.PerformUnitInput(unit, unit.QueuedInput);
	}

}
