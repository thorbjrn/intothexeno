﻿using System;
using System.Collections;
using IntoTheXeno;
using IntoTheXeno.Databases;
using UnityEngine;

public class MainFlow
{
	//event Action OnOpenMenu = () => { };

	private string queuedLevelId;

	public MainFlow()
	{
		Menus.OnStartLevel += OnStartLevel;
		//Menus.OnOpenMenu += OnOpenMenu;
	}

	private void OnStartLevel(string levelId)
	{
		Menus.CloseAll();

		queuedLevelId = levelId;

	}

	public IEnumerator Flow()
	{
		var mainMenu = Menus.Show<MainMenu>();
		yield return mainMenu.WaitForClose();

		queuedLevelId = "Level 4";
		Menus.CloseAll();
		yield return StartLevel();
	}

	private IEnumerator StartLevel(){
		Debug.Log("StartLevel - queuedLevelId: " + queuedLevelId);

		var heroRoster = new Roster { units = Boot.TEST_Heroes };
		var enemyRoster = new Roster { units = Boot.TEST_Enemies };
		var heroFactionInput = new HumanInput { factionId = 0 };
		var enemyFactionInput = new BotInput { factionId = 1 };

		var battleFlow = new BattleFlow(queuedLevelId, heroFactionInput, enemyFactionInput, heroRoster, enemyRoster);
		yield return battleFlow.Flow();
	}


}