﻿using System.Linq;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
	public Faction faction;

	[SerializeField] private Sprite[] sprites;

	public void Refresh()
	{
		int idx = (int)faction;
		var sprite = sprites[idx];
		GetComponentInChildren<SpriteRenderer>().sprite = sprite;
	}

	void OnEnable()
	{
		if (Application.isPlaying)
		{
			gameObject.SetActive(false);
		}
	}

	void OnValidate()
	{
		Refresh();
	}
}