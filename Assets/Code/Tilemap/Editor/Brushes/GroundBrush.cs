﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;


[CreateAssetMenu(fileName = "GroundBrush", menuName = "Brushes/Ground brush")]
[CustomGridBrush(true, true, true, "Ground Brush")]
public class GroundBrush : ALevelBrush
{

	public TileBase CurrentTile
	{
		get
		{
			var brushCell = cells.FirstOrDefault();
			if (brushCell == null) return null;

			return brushCell.tile;
		}
	}

	public override string TilemapName
	{
		get { return "Ground"; }
	}

	public override Type[] GetTileTypes(){
		return new Type[] { typeof(PlatformTile), typeof(WallTile) };
	}


	public override void Paint(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
	{
		//Debug.Log("Paint - gridLayout: " + gridLayout + ", brushTarget: " + brushTarget + ", position: " + position);
		//Debug.Log("cells: " + string.Join(", ", cells.Select(x => x.tile.name).ToArray()));

		base.Paint(gridLayout, brushTarget, position);
	}
}