﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public abstract class ALevelBrush : GridBrush
{
	public abstract string TilemapName { get; }

	public abstract Type[] GetTileTypes();
}


[CustomEditor(typeof(ALevelBrush), true)]
public class LevelBrushEditor : GridBrushEditor
{
	private ALevelBrush Target { get { return target as ALevelBrush; } }

	public override void OnPaintInspectorGUI()
	{

		//Debug.Log("OnPaintInspectorGUI - e.type: "+ Event.current.type);
		base.OnPaintInspectorGUI();
	}

	

	public override void OnToolActivated(GridBrushBase.Tool tool)
	{
		if (!Target.cells.All(x => x.tile != null && Target.GetTileTypes().Contains(x.tile.GetType() ) ) )
		{
			Debug.LogWarning("Brush is using invalid tile! Clearing cells");
			Target.UpdateSizeAndPivot(Vector3Int.zero, Vector3Int.zero);
			//Target.SetTile(Vector3Int.zero, null);

			return;
		}

		base.OnToolActivated(tool);
	}



	public override GameObject[] validTargets
	{
		get
		{
			var tilemapTarget = base.validTargets.FirstOrDefault(x => x.name == Target.TilemapName);
			return new GameObject[] { tilemapTarget };
		}
	}

	public override void OnPaintSceneGUI(GridLayout grid, GameObject brushTarget, BoundsInt position, GridBrushBase.Tool tool, bool executing)
	{
		base.OnPaintSceneGUI(grid, brushTarget, position, tool, executing);
	}
}