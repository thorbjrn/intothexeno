﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "LadderBrush", menuName = "Brushes/Ladder brush")]
[CustomGridBrush(true, false, false, "Ladder Brush")]
public class LadderBrush : ALevelBrush
{
	public override string TilemapName
	{
		get { return "Ladders"; }
	}

	public override Type[] GetTileTypes()
	{
		return new Type[] { typeof(LadderTile) };
	}
}