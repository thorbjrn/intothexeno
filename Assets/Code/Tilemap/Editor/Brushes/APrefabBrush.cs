﻿using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Prefab brush", menuName = "Brushes/Prefab brush")]
[CustomGridBrush(false, true, false, "Prefab Brush")]
public abstract class APrefabBrush : GridBrushBase
{
	protected abstract GameObject Prefab { get; }
	protected abstract Transform GetParent(GridLayout grid);


	public override void Paint(GridLayout grid, GameObject brushTarget, Vector3Int position)
	{
		Debug.Log("Paint grid: " + grid + ", brushTarget: " + brushTarget + ", position: " + position);
		if (brushTarget == null)
		{
			brushTarget = grid.transform.GetChild(0).gameObject;
		}

		// Do not allow editing palettes
		if (brushTarget.layer == 31)
			return;

		//var instance = (GameObject)PrefabUtility.InstantiatePrefab(Prefab);
		var instance = Instantiate(Prefab);
		if (instance != null)
		{
			Undo.MoveGameObjectToScene(instance, brushTarget.scene, "Paint Prefabs");
			Undo.RegisterCreatedObjectUndo((Object)instance, "Paint Prefabs");
			var parent = GetParent(grid);
			instance.transform.SetParent(parent);
			instance.transform.position = grid.LocalToWorld(grid.CellToLocalInterpolated(new Vector3Int(position.x, position.y, 0) + new Vector3(.5f, .5f, .5f)));
			PaintedPrefab(instance);
		}
	}

	public virtual void PaintedPrefab(GameObject instance){

	}

	public override void Erase(GridLayout grid, GameObject brushTarget, Vector3Int position)
	{
		Debug.Log("Erase grid: " + grid + ", brushTarget: " + brushTarget + ", position: " + position);
		if (brushTarget == null)
		{
			brushTarget = grid.transform.GetChild(0).gameObject;
		}
		// Do not allow editing palettes
		if (brushTarget.layer == 31)
			return;

		Transform erased = GetObjectInCell(grid, GetParent(grid), new Vector3Int(position.x, position.y, 0));
		if (erased != null)
			Undo.DestroyObjectImmediate(erased.gameObject);
	}

	private static Transform GetObjectInCell(GridLayout grid, Transform parent, Vector3Int position)
	{
		int childCount = parent.childCount;
		Vector3 min = grid.LocalToWorld(grid.CellToLocalInterpolated(position));
		Vector3 max = grid.LocalToWorld(grid.CellToLocalInterpolated(position + Vector3Int.one));
		Bounds bounds = new Bounds((max + min) * .5f, max - min);

		for (int i = 0; i < childCount; i++)
		{
			Transform child = parent.GetChild(i);
			if (bounds.Contains(child.position))
				return child;
		}
		return null;
	}
}