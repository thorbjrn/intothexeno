﻿using System;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "SpawnBrush", menuName = "Brushes/Spawn brush")]
[CustomGridBrush(true, false, false, "Spawn Brush")]
public class SpawnBrush : APrefabBrush
{
	[SerializeField] private SpawnPoint spawnPointPrefab;
	[SerializeField] private Faction faction;

	protected override GameObject Prefab
	{
		get { return spawnPointPrefab.gameObject; }
	}

	protected override Transform GetParent(GridLayout grid) {
		foreach (Transform child in grid.transform)
		{
			if (child.name == "SpawnPoints"){
				return child;
			}
		}
		return grid.transform;
	}


	public override void PaintedPrefab(GameObject instance)
	{
		instance.GetComponent<SpawnPoint>().faction = faction;
		instance.name = "Spawn_" + faction;
		instance.GetComponent<SpawnPoint>().Refresh();
	}
}


//[CustomEditor(typeof(SpawnBrush))]
//public class PrefabBrushEditor : GridBrushEditorBase
//{
//	private SpawnBrush Target { get { return target as SpawnBrush; } }

//	private SerializedProperty m_Prefabs;
//	private SerializedObject m_SerializedObject;

//	//protected void OnEnable()
//	//{
//	//	m_SerializedObject = new SerializedObject(target);
//	//	m_Prefabs = m_SerializedObject.FindProperty("m_Prefabs");
//	//}

//	//public override void OnPaintInspectorGUI()
//	//{
//	//	m_SerializedObject.UpdateIfRequiredOrScript();
//	//	//Target.m_PerlinScale = EditorGUILayout.Slider("Perlin Scale", Target.m_PerlinScale, 0.001f, 0.999f);
//	//	//Target.m_Z = EditorGUILayout.IntField("Position Z", Target.m_Z);

//	//	EditorGUILayout.PropertyField(m_Prefabs, true);
//	//	m_SerializedObject.ApplyModifiedPropertiesWithoutUndo();
//	//}
//}