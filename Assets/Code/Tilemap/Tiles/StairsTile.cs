﻿using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class StairsTile : Tile
{
	public bool facingRight;

#if UNITY_EDITOR
	[MenuItem("Assets/Create/StairsTile")]
	public static void Create()
	{
		string path = EditorUtility.SaveFilePanelInProject("Save Tile", "NewTile", "Asset", "Save Tile", "Assets");
		if (path == "")
			return;
		AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<StairsTile>(), path);
	}
#endif
}

