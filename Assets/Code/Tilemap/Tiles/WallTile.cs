﻿using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class WallTile : Tile 
{

#if UNITY_EDITOR
	[MenuItem("Assets/Create/WallTile")]
    public static void Create()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Tile", "NewTile", "Asset", "Save Tile", "Assets");
        if (path == "")
            return;
    	AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<WallTile>(), path);
    }
#endif
}