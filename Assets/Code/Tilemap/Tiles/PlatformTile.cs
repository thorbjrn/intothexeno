﻿using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PlatformTile : Tile 
{
    //public Sprite[] m_Sprites;
    //public Sprite m_Preview;
    
	
	// This refreshes itself and other RoadTiles that are orthogonally and diagonally adjacent
  //  public override void RefreshTile(Vector3Int location, ITilemap tilemap)
  //  {
		//base.RefreshTile(location, tilemap);
    //}

 //   public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
	//{
	//	//base.GetTileData(position, tilemap, ref tileData);

	//	tileData.sprite = sprite;
	//	var tileTransform = tileData.transform;
	//	tileTransform.SetTRS(Vector3.forward * 0.1f, Quaternion.identity, Vector3.one);

	//	tileData.transform = tileTransform;
	//	tileData.colliderType = ColliderType.None;
	//	tileData.color = Color.white;
	//	tileData.flags = TileFlags.LockTransform;
	//}

#if UNITY_EDITOR
	[MenuItem("Assets/Create/PlatformTile")]
    public static void Create()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Tile", "New Tile", "Asset", "Save Tile", "Assets");
        if (path == "")
            return;
    	AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<PlatformTile>(), path);
    }
#endif
}