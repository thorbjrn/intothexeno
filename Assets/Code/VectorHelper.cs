﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class VectorHelper
{
	public static List<Vector2Int> PositionsBetween(Vector2Int start, Vector2Int end){
		var positions = new List<Vector2Int>();
		var diff = end - start;
		if (diff.x == 0 ^ diff.y == 0){
			var dir = diff.Normalized();
			var pos = start + diff;
			int derp = 0;
			while (pos != end && derp++ < 100)
			{
				positions.Add(pos);
				pos += dir;
			}
			positions.Add(end);
		}
		else{
			Debug.LogError("Invalid diff!: " + diff);
		}
		return positions;
	}
}
