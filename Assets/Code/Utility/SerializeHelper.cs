﻿using System;
using System.Linq;
using UnityEngine;

public static class SerializeHelper
{
	public static T Deserialize<T>(string serializedType, string serializedFields)
	{
		var obj = default(T);

		//Debug.Log("Creating target - serializedType: " + serializedType + ", fields: " + serializedFields);
		try
		{
			var t = Type.GetType(serializedType);
			obj = (T)Activator.CreateInstance(t);
			var targetTypeFields = Type.GetType(serializedType).GetFields();
			var fields = serializedFields.Split(',');
			foreach (var field in fields)
			{
				try
				{
					var fieldName = field.Split(':')[0];
					var fieldValue = field.Split(':')[1];
					var fieldInfo = targetTypeFields.FirstOrDefault(x => x.Name == fieldName);
					if (fieldInfo.FieldType == typeof(System.Int32))
					{
						fieldInfo.SetValue(obj, int.Parse(fieldValue));
					}
					else if (fieldInfo.FieldType == typeof(System.Single))
					{
						fieldInfo.SetValue(obj, float.Parse(fieldValue));
					}
					else if (fieldInfo.FieldType == typeof(System.String))
					{
						fieldInfo.SetValue(obj, fieldValue);
					}
					else if (fieldInfo.FieldType == typeof(System.Boolean))
					{
						fieldInfo.SetValue(obj, bool.Parse(fieldValue));
					}
					else if (fieldInfo.FieldType == typeof(Vector2Int))
					{
						var val = new Vector2Int(
							int.Parse(fieldValue.Split('_')[0]),
							int.Parse(fieldValue.Split('_')[1])
						);
						fieldInfo.SetValue(obj, val);
					}
				}
				catch { }


			}
		}
		catch (Exception e)
		{
			Debug.LogError(e);
		}

		return obj;

	}
}
