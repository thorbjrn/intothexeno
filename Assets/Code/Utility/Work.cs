﻿using System;
using System.Collections;
using UnityEngine;

public class Work
{
	public object result;
	//private IEnumerator target;

	public IEnumerator Run(IEnumerator target)
	{
		//this.target = target;
		while (target.MoveNext())
		{
			result = target.Current;
			yield return result;
		}
	}
}

public class Work<T>
{
	public T result;
	//private IEnumerator target;

	public IEnumerator Run(IEnumerator target)
	{
		//this.target = target;
		while (target.MoveNext())
		{
			//Debug.Log("target.Current: " + target.Current + ", " + (target.Current != null ? target.Current.GetType().ToString() : ""));
			if (target.Current is T){
				result = (T)target.Current;
			}
			yield return target.Current;
		}
	}
}