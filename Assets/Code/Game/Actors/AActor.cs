﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AActor
{
	static int actorCounter;

	private int actorId = actorCounter++;
	public int ActorId {
		get{
			return actorId;
		}
		set {
			actorId = value;
		}
	}

	public Vector2Int Position {
		get { return ActorPiece.Position; }
		set { ActorPiece.Position = value; }
	}

	public int Health { get; protected set; }
	private bool isAlive;
	public bool IsAlive {
		get { return isAlive; }
		set {
			isAlive = value;
			if (!isAlive && ActorPiece is ActorPiece) EventManager.OnActorDead(this);
		}
	}
	public abstract int MaxHealth { get; }
	public IActorPiece ActorPiece { get; set; }
	public abstract int FactionId { get; }

	public override string ToString()
	{
		return ActorPiece + " - pos: " + Position;
	}

	internal void TakeDamage(int damage)
	{
		Health = Mathf.Clamp(Health - damage, 0, int.MaxValue);

		if (ActorPiece is ActorPiece)
		{
			EventManager.OnUpdateActorHealth(this);
		}
	}

	public abstract AActor GetCopy();

	public override int GetHashCode()
	{
		return ActorId;
	}
}
