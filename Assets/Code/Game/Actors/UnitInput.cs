﻿using System.Collections.Generic;
using UnityEngine;

public class UnitInput
{
    public InputType inputType;
    public List<Vector2Int> targetPositions;
	public UnitActionDefinition unitAction;

	public override string ToString()
	{
		return ObjectDumper.Dump(this, 1);
	}
}

public enum InputType
{
    MOVE,
    ACTION,
    END_TURN,
	CANCEL,
}