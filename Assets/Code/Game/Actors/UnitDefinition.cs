﻿using System;
using UnityEngine;

[Serializable]
public class UnitDefinition
{
	public string unitType;
	public UnitData unitData;
	public AnimatorOverrideController animController;
}
