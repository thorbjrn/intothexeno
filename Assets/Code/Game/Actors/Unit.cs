﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using IntoTheXeno.Databases;
using UnityEngine;

public class Unit : AActor
{
	public bool HasMoved { get; set; }
	public float ActionInitiative { get; set; }
	public UnitData Data { get; internal set; }
	public FactionInput FactionInput { get; private set; }

	public override int FactionId {
		get { return FactionInput.factionId; }
	}

	public List<UnitActionDefinition> UnitActionsDefinitions
	{
		get
		{
			var database = UnitActionDatabase.Get<UnitActionDatabase>();
			return database.GetUnitActionDefinitions(Data.actionIds);
		}
	}

	public override int MaxHealth
	{
		get { return Data.maxHealth; }
	}

	public UnitInput QueuedInput { get; set; }

	public void Initialize(FactionInput factionInput, Vector2 spawnPoint)
	{
		FactionInput = factionInput;

		Health = Data.maxHealth;
		IsAlive = true;

		Position = spawnPoint.ToInt();

		if (ActorPiece is ActorPiece){
			EventManager.OnUpdateActorHealth(this);
		}
	}

	internal void StartTurn()
	{
		HasMoved = false;
		ActorPiece.IsActive = true;
	}

	internal UnitActionDefinition PrepareAction(int useActiondIdx)
	{
		var actionId = Data.actionIds[useActiondIdx];
		var unitActionDefintions = ADatabase.Get<UnitActionDatabase>().GetUnitActionDefinitions(Data.actionIds);

		EventManager.OnShowUnitActions(unitActionDefintions, new bool[] { useActiondIdx == 0, useActiondIdx == 1 });

		return unitActionDefintions.FirstOrDefault(x => x.actionId == actionId);
	}

	public void HideActionTargets()
	{
		var unitActionDefintions = ADatabase.Get<UnitActionDatabase>().GetUnitActionDefinitions(Data.actionIds);

		EventManager.OnShowUnitActions(unitActionDefintions, new bool[] { false, false });
	}

	public override AActor GetCopy()
	{
		var actorPieceCopy = new FakeActorPiece
		{
			IsActive = ActorPiece.IsActive,
			Position = ActorPiece.Position
		};

		var copy = new Unit
		{
			HasMoved = HasMoved,
			Data = Data,
			FactionInput = FactionInput,
			Health = Health,
			ActorPiece = actorPieceCopy
		};
		copy.IsAlive = IsAlive;
		copy.Position = Position;
		copy.ActorId = ActorId;

		return copy;
	}
}
