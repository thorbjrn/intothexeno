﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UnitData
{
    public int maxHealth;
    public int movePoints;
	public int initative;
    public string[] actionIds;
}
