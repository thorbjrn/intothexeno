﻿using IntoTheXeno.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ActorController
{
	const int QueuedUnitCount = 10;

	private int queuedTurnNumber;

	public List<AActor> Actors { get; private set; }
	public Queue<Unit> QueuedUnits { get; private set; }

	public ActorController()
	{
		QueuedUnits = new Queue<Unit>();
		Actors = new List<AActor>();
	}

	public void CreateUnits(FactionInput factionInput, Roster roster, List<Vector2> spawnPoints)
	{
		foreach (var rosterUnit in roster.units)
		{
			var spawnPoint = spawnPoints[spawnPoints.Count - 1];
			spawnPoints.Remove(spawnPoint);

			var unit = ADatabase.Get<UnitDatabase>().CreateUnit(rosterUnit.unitType);
			unit.Initialize(factionInput, spawnPoint);

			Actors.Add(unit);
		}
	}

	public void SortUnits()
	{
		var units = Actors.Where(x => x.GetType() == typeof(Unit)).Select(x => x as Unit);

		if (queuedTurnNumber == 0)
		{
			foreach (var unit in units)
			{
				unit.ActionInitiative = 10f / unit.Data.initative;
			}
		}

		while (QueuedUnits.Count < QueuedUnitCount)
		{
			queuedTurnNumber++;

			foreach (var unit in units)
			{
				unit.ActionInitiative -= 1f;
			}

			int derpCounter = 0;
			while (derpCounter++ < 1000 && units.Any(x => x.ActionInitiative <= 1f))
			{
				foreach (var unit in units.OrderBy(x => x.ActionInitiative + UnityEngine.Random.Range(-0.0001f, 0.0001f)))
				{
					if (unit.ActionInitiative <= 1f)
					{
						unit.ActionInitiative += 10f / unit.Data.initative;

						QueuedUnits.Enqueue(unit);
					}
				}
			}
		}
	}

	public IEnumerator Conjure(EffectResult effectResult, int factionId, bool isSimulation)
	{
		//Debug.Log("Conjure - id: " + effectResult.conjureId);

		var obstacle = ADatabase.Get<ObstacleDatabase>().CreateObstacle(effectResult.conjureId, isSimulation);
		obstacle.Initialize(effectResult.position, factionId);

		Actors.Add(obstacle);

		yield break;
	}

	public IEnumerator DealDamage(AActor actor, int damage)
	{
		//Debug.Log("DealDamage - actor: " + actor + ", damage: " + damage);

		if (damage == 0) yield break;

		actor.TakeDamage(damage);
	}

	public AActor GetActorAtPosition(Vector2Int pos)
	{
		return Actors.FirstOrDefault(x => x.Position == pos && x.IsAlive);
	}

	public ActorController CreateFake()
	{
		var fakeActors = new List<AActor>();
		foreach (var actor in Actors)
		{
			var fakeActor = actor.GetCopy();
			fakeActors.Add(fakeActor);
		}

		var fakeActorController = new ActorController
		{
			Actors = fakeActors
		};

		return fakeActorController;
	}
}