﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActorPiece
{
	bool IsActive { get; set; }
	Vector2Int Position { get; set; }

	IEnumerator MoveToPosition(Vector3 pos);
	IEnumerator MoveOnPath(Path path);
	IEnumerator BounceOnPath(Path bouncePath);
	IEnumerator Die();
}
