﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ActorPiece : MonoBehaviour, IActorPiece
{
	[SerializeField] SpriteRenderer _spriteRenderer;
	[SerializeField] Animator animator;

	int animHorizontalDirection = Animator.StringToHash("HorizontalDirection");
	int animVerticalDirection = Animator.StringToHash("VerticalDirection");
	int animSpeed = Animator.StringToHash("Speed");

	bool isActive;

	public bool IsActive {
		get { return isActive; }
		set {
			_spriteRenderer.color = value ? Color.red : Color.white;
			isActive = value;
		}
	}

	public Vector2Int Position {
		get {
			return this.transform.position.ToInt();
		}

		set {
			this.transform.position = value.ToVector3();
		}
	}

	void Start()
	{
		if (animator != null){
			animator.SetInteger(animHorizontalDirection, 1);
			animator.SetFloat(animSpeed, 0f);
		}
	}

	public IEnumerator MoveToPosition(Vector3 pos)
	{
		int horizontalDir = Mathf.RoundToInt(pos.x - transform.position.x);
		int verticalDir = Mathf.RoundToInt(pos.y - transform.position.y);

		if (animator) animator.SetInteger(animHorizontalDirection, horizontalDir);
		if (animator) animator.SetInteger(animVerticalDirection, verticalDir);

		while (transform.position != pos)
		{
			const float SPEED = 5f;

			if (animator) animator.SetFloat(animSpeed, SPEED);
			transform.position = Vector2.MoveTowards(transform.position, pos, Time.deltaTime * SPEED);
			yield return null;

		}

		//animator.SetInteger(animHorizontalDirection, horizontalDir);
		//animator.SetInteger(animVerticalDirection, 0);
		if (animator) animator.SetFloat(animSpeed, 0f);
	}

	public IEnumerator MoveOnPath(Path path)
	{
		for (int i = 1; i < path.positions.Count; i++)
		{
			var goalPosition = path.positions[i];

			yield return MoveToPosition(goalPosition.ToVector3());
		}
	}

	public IEnumerator BounceOnPath(Path bouncePath)
	{
		float xDirection = bouncePath.positions[0].x - bouncePath.positions[1].x;
		var goalPosition = bouncePath.positions[0];
		yield return MoveToPosition(new Vector3(goalPosition.x + 0.5f - xDirection * 0.5f, goalPosition.y + 0.5f, 0f));

		goalPosition = bouncePath.positions[1];
		yield return MoveToPosition(goalPosition.ToVector3());
	}

	public void SetAnimController(AnimatorOverrideController overrideController)
	{
		animator.runtimeAnimatorController = overrideController;
	}

	public IEnumerator Die()
	{
		Debug.Log("Actor " + this + " is dead!");
		
		transform.localScale = new Vector3(0f, 0f, 1f);

		yield return new WaitForSeconds(0.5f);
	}
}
