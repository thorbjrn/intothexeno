﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Path
{
	public List<Vector2Int> positions = new List<Vector2Int>();

	public Path Copy()
	{
		var positionsCopy = new List<Vector2Int>(positions);
		var path = new Path { positions = positionsCopy };
		return path;
	}

	public override string ToString()
	{
		return "Positions: " + string.Join(", ", positions.Select(x => x.ToString()).ToArray());
	}
}