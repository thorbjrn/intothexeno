﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : AActor
{
	public int factionId;
	public override int FactionId
	{
		get { return factionId; }
	}
	private int startHealth;

	public override int MaxHealth
	{
		get { return startHealth; }
	}

	public void Spawn(int health)
	{
		startHealth = health;
		Health = startHealth;
		IsAlive = true;
	}

	internal void Initialize(Vector2Int position, int factionId)
	{
		this.factionId = factionId;
		Position = position;

		if (ActorPiece is ActorPiece)
		{
			EventManager.OnUpdateActorHealth(this);
		}
	}

	public override AActor GetCopy()
	{
		var actorPieceCopy = new FakeActorPiece
		{
			IsActive = ActorPiece.IsActive,
			Position = ActorPiece.Position
		};

		var copy = new Obstacle
		{
			Health = Health,
			ActorPiece = actorPieceCopy,
			factionId = factionId,
		};
		copy.IsAlive = IsAlive;
		copy.Position = Position;
		copy.ActorId = ActorId;


		return copy;
	}
}
