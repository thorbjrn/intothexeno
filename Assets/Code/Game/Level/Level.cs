﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class Level : MonoBehaviour
{
    [SerializeField] Tilemap groundMap;
	[SerializeField] Tilemap ladderMap;

	public List<Vector2> GetHeroSpawnPoints()
    {
		var spawnPoints = GetComponentsInChildren<SpawnPoint>(true);
        var positions = new List<Vector2>();
		foreach (var point in spawnPoints.Where(x => x.faction == Faction.TEAM_1))
        {
			positions.Add(point.transform.localPosition);
        }
        return positions;
    }

	public List<Vector2> GetEnemySpawnPoints()
	{
		var spawnPoints = GetComponentsInChildren<SpawnPoint>(true);
		var positions = new List<Vector2>();
		foreach (var point in spawnPoints.Where(x => x.faction == Faction.TEAM_2))
		{
			positions.Add(point.transform.localPosition);
		}
		positions.OrderBy(x => Random.Range(int.MinValue, int.MaxValue)).ToList();

		return positions;
	}

	public bool HasPlatformAt(Vector2Int pos){
		var tile = groundMap.GetTile(pos);
		return tile is PlatformTile;
	}

	public bool HasWallAt(Vector2Int pos)
	{
		var tile = groundMap.GetTile(pos);
		return tile is WallTile;
	}

	public bool HasLadderAt(Vector2Int pos)
	{
		var tile = ladderMap.GetTile(pos);
		return tile is LadderTile;
	}

	public bool HasStairsAt(Vector2Int pos, bool facingRight)
	{
		var tile = ladderMap.GetTile(pos);
		var stairsTile = tile as StairsTile;
		return stairsTile != null && stairsTile.facingRight == facingRight;
	}
}
