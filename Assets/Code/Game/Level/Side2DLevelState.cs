﻿using IntoTheXeno.Databases;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Side2DLevelState : ILevelState
{
	public Level Level { get; private set; }

	private ActorController actorController;

	const int FallDamageInitial = 0;
	const int FallDamagePerTile = 1;
	const int SafeFallHeight = 2;

	public void Initialize(Level level, ActorController actorController)
	{
		Level = level;
		this.actorController = actorController;
	}

	public List<Vector2Int> GetNeighbourTiles(Vector2Int pos)
	{
		var neighbourTilePositions = new List<Vector2Int>();

		//Return instantly if not standing on anything
		if (!HasGroundAt(pos) && !Level.HasLadderAt(pos)) return neighbourTilePositions;

		//Can move horizontally?
		var horizontalPositions = new[] { pos + Vector2Int.left, pos + Vector2Int.right };
		foreach (var horizontalPos in horizontalPositions)
		{
			if ((Level.HasLadderAt(horizontalPos) || HasGroundAt(horizontalPos)) && !Level.HasWallAt(horizontalPos))
			{
				neighbourTilePositions.Add(horizontalPos);
			}
//			else if (HasGroundAt(pos) && !Level.HasWallAt(horizontalPos)) //Can walk off ledges
//			{
//				neighbourTilePositions.Add(horizontalPos);
//			}
		}

		//Can go up?
		var abovePos = pos + Vector2Int.up;
		if (Level.HasLadderAt(pos))
		{
			if (!Level.HasWallAt(abovePos) && (HasGroundAt(abovePos) || Level.HasLadderAt(abovePos)))
			{
				neighbourTilePositions.Add(abovePos);
			}
		}

		//Can go down?
		var belowPos = pos + Vector2Int.down;
		if (Level.HasLadderAt(belowPos))
		{
			neighbourTilePositions.Add(belowPos);
		}

		//Can move up on stairs?
		foreach (var horizontalPos in horizontalPositions)
		{
			bool movingRight = horizontalPos.x > pos.x;
			if (Level.HasStairsAt(horizontalPos, movingRight))
			{
				var upHorizontalPos = horizontalPos + Vector2Int.up;
				if (!Level.HasWallAt(upHorizontalPos) && (HasGroundAt(upHorizontalPos) || Level.HasLadderAt(upHorizontalPos)))
				{
					neighbourTilePositions.Add(upHorizontalPos);
				}
			}
		}

		//Can move down on stairs?
		foreach (var horizontalPos in horizontalPositions)
		{
			bool movingRight = horizontalPos.x > pos.x;
			if (Level.HasStairsAt(belowPos, !movingRight))
			{
				var downHorizontalPos = horizontalPos + Vector2Int.down;
				if (HasGroundAt(downHorizontalPos))
				{
					neighbourTilePositions.Add(downHorizontalPos);
				}
			}
		}

		return neighbourTilePositions;
	}

	public Vector2Int GetGroundBelowPos(Vector2Int startPos)
	{
		const int maxTilesToFall = 10;

		var pos = startPos;
		int tilesFell = 0;
		while (tilesFell <= maxTilesToFall)
		{
			//Debug.Log("GetActorAtPosition - pos: " + pos + ", GetActorAtPosition(pos): " + GetActorAtPosition(pos) + " , GetActorAtPosition(pos+down): " + GetActorAtPosition(pos+Vector2Int.down));
			if (HasGroundAt(pos)) // || GetActorAtPosition(pos + Vector2Int.down) != null)
			{
				break;
			}
			pos += Vector2Int.down;
			tilesFell++;
		}
		return pos;
	}

	private bool HasGroundAt(Vector2Int pos)
	{
		var posBelow = pos + Vector2Int.down;
		return Level.HasPlatformAt(posBelow) || Level.HasWallAt(posBelow);
	}

	public IEnumerator PostUnitInputRoutine()
	{
		yield return ApplyGravity();
	}

	public ILevelState CreateFake(ActorController fakeActorController)
	{
		var fake = new Side2DLevelState
		{
			Level = Level,
			actorController = fakeActorController
		};

		return fake;
	}

	private IEnumerator ApplyGravity()
	{
		foreach (var actor in actorController.Actors)
		{
			var pos = actor.Position;

			Path fallPath = null;
			var groundPos = GetGroundBelowPos(pos);
			AActor squashedActor = null;
			if (groundPos != pos)
			{
				squashedActor = actorController.GetActorAtPosition(groundPos);
				fallPath = new Path { positions = new List<Vector2Int> { pos, groundPos } };
			}

			if (fallPath != null)
			{
				actor.ActorPiece.IsActive = false;
				yield return actor.ActorPiece.MoveOnPath(fallPath);

				if (squashedActor != null)
				{
					yield return actorController.DealDamage(squashedActor, squashedActor.Health);
				}

				int unsafeFallHeight = (int)(groundPos - pos).magnitude - SafeFallHeight;
				if (unsafeFallHeight > 0)
				{
					int damage = FallDamageInitial + FallDamagePerTile * unsafeFallHeight;
					yield return actorController.DealDamage(actor, damage);
				}
			}
		}
	}
}