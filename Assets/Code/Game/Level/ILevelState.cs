﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILevelState
{
	Level Level { get; }

	void Initialize(Level level, ActorController actorController);
	List<Vector2Int> GetNeighbourTiles(Vector2Int pos);
	IEnumerator PostUnitInputRoutine();
	ILevelState CreateFake(ActorController fakeActorController);

}