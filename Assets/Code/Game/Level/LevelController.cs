﻿using IntoTheXeno.Databases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelController
{
	const int BounceUnitDamage = 1;

	public ILevelState LevelState { get; private set; }
	public ActorController ActorController { get; private set; }
	public int RoundNumber { get; internal set; }

	private int turnNumber;

	public LevelController(){ }

	public LevelController(ILevelState levelState, Level level, FactionInput faction1, FactionInput faction2, Roster roster1, Roster roster2)
	{
		LevelState = levelState;
		ActorController = new ActorController();
		levelState.Initialize(level, ActorController);

		ActorController.CreateUnits(faction1, roster1, level.GetHeroSpawnPoints());
		ActorController.CreateUnits(faction2, roster2, level.GetEnemySpawnPoints());
	}

	public IEnumerator PerformUnitInput(Unit unit, UnitInput unitInput)
	{
		var possiblePaths = GetPossiblePaths(unit);
		//Debug.Log("PerformUnitInput - unit: " + unit + ", input: " + unitInput + ", possiblePaths: "+ ObjectDumper.Dump(possiblePaths, 2));

		switch (unitInput.inputType)
		{
		case InputType.MOVE:
			var path = possiblePaths[unitInput.targetPositions.First()];
			unit.HasMoved = true;

			yield return unit.ActorPiece.MoveOnPath(path);
			break;
		case InputType.ACTION:
			yield return PerformActionEffects(unit, unitInput);

			unit.ActorPiece.IsActive = false;
			break;
		case InputType.END_TURN:
			unit.ActorPiece.IsActive = false;
			break;
		}

		yield return LevelState.PostUnitInputRoutine();
		yield return KillOutOfHealthActors();
	}

	public Unit GetNextUnitByInitiative()
	{
		ActorController.SortUnits();
		//Debug.Log("Round " + levelState.RoundNumber + " started");
		var unit = ActorController.QueuedUnits.Dequeue();
		return unit;
	}

	private IEnumerator PerformActionEffects(Unit unit, UnitInput unitInput)
	{
		//Debug.Log("PerformActionEffects - unit: " + unit + ", input: " + unitInput);

		var action = unitInput.unitAction;
		foreach (var effectResult in action.GetEffectResults(unit.Position, unitInput.targetPositions))
		{
			bool isSimulation = unit.ActorPiece is FakeActorPiece;
			yield return PerformActionEffect(effectResult, unit.FactionInput.factionId, isSimulation);
		}
	}

	private IEnumerator PerformActionEffect(EffectResult effectResult, int factionId, bool isSimulation)
	{
		//Debug.Log("PerformActionEffect - result: " + effectResult);

		if (!string.IsNullOrEmpty(effectResult.conjureId))
		{
			yield return ActorController.Conjure(effectResult, factionId, isSimulation);
		}

		var targetActor = ActorController.GetActorAtPosition(effectResult.position);
		if (targetActor == null) yield break;

		yield return ActorController.DealDamage(targetActor, effectResult.damage);

		if (targetActor is Unit)
		{
			yield return ForceMoveUnit(targetActor as Unit, effectResult);
		}
	}

	private IEnumerator ForceMoveUnit(Unit targetUnit, EffectResult effectResult)
	{
		if (effectResult.forceMove == Vector2Int.zero) yield break;

		var endPos = targetUnit.Position + effectResult.forceMove;
		var positions = VectorHelper.PositionsBetween(targetUnit.Position, endPos);
		var path = new Path { positions = new List<Vector2Int> { targetUnit.Position } };
		Path bouncePath = null;

		//Loop through positions, and check for collisions with units, walls, etc.
		foreach (var pos in positions)
		{
			var hasUnitAtPos = ActorController.GetActorAtPosition(pos) != null;
			var hasWallAtPos = LevelState.Level.HasWallAt(pos);
			if (hasUnitAtPos || hasWallAtPos)
			{
				var dir = (pos - targetUnit.Position).Normalized();
				endPos = pos - dir;
				bouncePath = new Path { positions = new List<Vector2Int> { pos, endPos } };
				break;
			}

			path.positions.Add(pos);
		}

		yield return targetUnit.ActorPiece.MoveOnPath(path);

		if (bouncePath != null)
		{
			yield return targetUnit.ActorPiece.BounceOnPath(bouncePath);

			yield return ActorController.DealDamage(targetUnit, BounceUnitDamage);

			var actorAtPos = ActorController.GetActorAtPosition(bouncePath.positions[0]);
			if (actorAtPos != null)
			{
				yield return ActorController.DealDamage(actorAtPos, BounceUnitDamage);
			}
		}
	}





	private IEnumerator KillOutOfHealthActors()
	{
		foreach (var actor in ActorController.Actors)
		{
			if (actor.IsAlive && actor.Health <= 0)
			{
				actor.IsAlive = false;
				yield return actor.ActorPiece.Die();
			}
		}
	}

	public LevelController CreateFake()
	{
		var fakeActorController = ActorController.CreateFake();
		var fakeLevelState = LevelState.CreateFake(fakeActorController);

		var fake = new LevelController
		{
			ActorController = fakeActorController,
			LevelState = fakeLevelState
		};

		return fake;
	}

	public Dictionary<Vector2Int, Path> GetPossiblePaths(Unit unit)
	{
		var validPaths = GetValidPaths(unit);
		validPaths = validPaths.OrderBy(x => x.positions.Count).ToList();

		var possiblePaths = new Dictionary<Vector2Int, Path>();
		foreach (var path in validPaths)
		{
			var endPos = path.positions.Last();
			if (possiblePaths.ContainsKey(endPos)) continue;
			if (ActorController.GetActorAtPosition(endPos) != null) continue;
			//if (!HasGroundAt(endPos)) continue;

			possiblePaths.Add(endPos, path);
		}
		return possiblePaths;
	}

	public List<Path> GetValidPaths(Unit unit)
	{
		var startPath = new Path { positions = { unit.Position } };
		var paths = GetPaths(startPath, unit.Data.movePoints);

		return paths;
	}

	public List<Path> GetPaths(Path path, int maxMoves)
	{
		//Debug.Log("GetPaths - path: " + path + ", maxMoves: "+ maxMoves);

		var paths = new List<Path>();
		var lastPos = path.positions.Last();
		var neighbourPositions = LevelState.GetNeighbourTiles(lastPos);
		foreach (var pos in neighbourPositions)
		{
			if (path.positions.Contains(pos)) continue;

			var newPath = path.Copy();
			newPath.positions.Add(pos);
			paths.Add(newPath);

			if (newPath.positions.Count <= maxMoves)
			{
				var nextLayerPaths = GetPaths(newPath, maxMoves);
				paths.AddRange(nextLayerPaths);
			}
		}

		return paths;
	}



	public List<Vector2Int> GetValidTargetPositions(Vector2Int actorPos, TargetResult targetResult)
	{
		var validTargetPositions = new List<Vector2Int>();
		foreach (var targetPos in targetResult.possiblePositions)
		{
			bool isValidPosition = !IsLineBlocked(actorPos, targetPos, targetResult.targetType.IsBlockedByUnits);
			isValidPosition &= !LevelState.Level.HasWallAt(targetPos);
			isValidPosition &= (targetResult.targetType.CanTargetUnits || ActorController.GetActorAtPosition(targetPos) == null);
			isValidPosition &= (targetResult.targetType.CanTargetEmpty || ActorController.GetActorAtPosition(targetPos) != null);

			if (isValidPosition) validTargetPositions.Add(targetPos);
		}

		return validTargetPositions;
	}


	public bool IsLineBlocked(Vector2Int start, Vector2Int end, bool checkActors)
	{
		//Debug.Log("IsLineBlocked - start: " + start + ", end: " + end);
		var dir = (end - start).Normalized();
		var currPos = start + dir;
		int derp = 0;
		while (currPos != end && derp++ < 100)
		{
			if (checkActors && ActorController.GetActorAtPosition(currPos) != null) return true;
			if (LevelState.Level.HasWallAt(currPos)) return true;
			currPos += dir;
		}
		return false;
	}

	public List<Unit> GetUnitsOfFaction(int factionId)
	{
		return ActorController.Actors.Where(x => x is Unit && x.FactionId == factionId).Select(x => x as Unit).ToList();
	}
}
