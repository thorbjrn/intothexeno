﻿using IntoTheXeno.Databases;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TopDown2DLevelState : ILevelState
{
	public Level Level { get; private set; }

	private ActorController actorController;


	public void Initialize(Level level, ActorController actorController)
	{
		Level = level;
		this.actorController = actorController;
	}

	public List<Vector2Int> GetNeighbourTiles(Vector2Int pos)
	{
		var neighbourTilePositions = new List<Vector2Int>();

		//Can move?
		var neighbourPositions = new Vector2Int[] { pos + Vector2Int.left, pos + Vector2Int.right, pos + Vector2Int.up, pos + Vector2Int.down };
		foreach (var neighbourPos in neighbourPositions)
		{ 
			if (!Level.HasWallAt(neighbourPos))
			{
				neighbourTilePositions.Add(neighbourPos);
			}
		}

		return neighbourTilePositions;
	}


	public ILevelState CreateFake(ActorController fakeActorController)
	{
		var fake = new TopDown2DLevelState
		{
			Level = Level,
			actorController = fakeActorController
		};

		return fake;
	}

	public IEnumerator PostUnitInputRoutine()
	{
		yield break;
	}
}