﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FakeActorPiece : IActorPiece
{
	public bool IsActive {
		get; set;
	}

	public Vector2Int Position { get; set; }

	public IEnumerator BounceOnPath(Path bouncePath)
	{
		yield break;
	}

	public IEnumerator Die()
	{
		yield break;
	}

	public IEnumerator MoveOnPath(Path path)
	{
		Position = path.positions.Last();
		yield break;
	}

	public IEnumerator MoveToPosition(Vector3 pos)
	{
		Position = pos.ToInt();
		yield break;
	}
}
