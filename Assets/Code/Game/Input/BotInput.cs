﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BotInput : FactionInput
{
	public override IEnumerator GetUnitInput(Unit unit, InputType[] validInputTypes)
	{
		UnitInput unitInput = null;

		//Evaluate level state heuristic score
		float currentScore = EvaluateLevelState(LevelController);

		var possibleInputs = GetPossibleInputs(unit, validInputTypes);

		//Find input that leads to best level state score
		//	simulate level state results
		//	evaluate level state heuristic score from simulation
		UnitInput bestInput = null;
		float highestScore = float.MinValue;
		foreach (var input in possibleInputs)
		{
			var simulatedLevelController = GetSimulatedLevelState(LevelController, unit.ActorId, input);
			float simulationScore = EvaluateLevelState(simulatedLevelController);

			var simulatedUnit = (Unit)simulatedLevelController.ActorController.Actors.First(x => x.ActorId == unit.ActorId);
			if (simulatedUnit.ActorPiece.IsActive){
				var possibleSecondInputs = GetPossibleInputs(simulatedUnit, new[] {InputType.MOVE, InputType.ACTION, InputType.END_TURN});
				foreach (var secondInput in possibleSecondInputs)
				{
					var secondSimulatedLevelState = GetSimulatedLevelState(simulatedLevelController, unit.ActorId, secondInput);
					float secondSimulationScore = EvaluateLevelState(secondSimulatedLevelState);
					if (secondSimulationScore > simulationScore)
					{
						simulationScore = secondSimulationScore;
					}
				}
			}

			if (simulationScore > highestScore){
				bestInput = input;
				highestScore = simulationScore;
			}
		}

		unitInput = bestInput;
		unitInput.inputType = bestInput.inputType;

		Debug.Log("Bot decided on move: " + unitInput + ", previous score: " + currentScore + ", new score: " + highestScore);

		yield return unitInput;
	}

	private List<UnitInput> GetPossibleInputs(Unit unit, InputType[] validInputTypes)
	{
		var possibleInputs = new List<UnitInput>();
		if (validInputTypes.Contains(InputType.MOVE)) possibleInputs.AddRange(GetMovementsActs(unit)); //Add input for each possible move
		if (validInputTypes.Contains(InputType.ACTION)) possibleInputs.AddRange(GetActionActs(unit)); //Add input for each possible action target
		possibleInputs.Add(new UnitInput { inputType = InputType.END_TURN });
		return possibleInputs;
	}

	private LevelController GetSimulatedLevelState(LevelController levelController, int actorId, UnitInput input)
	{
		var simulatedLevelController = levelController.CreateFake();
		var simulatedUnit = (Unit)simulatedLevelController.ActorController.Actors.First(x => x.ActorId == actorId);
		var enumerator = simulatedLevelController.PerformUnitInput(simulatedUnit, input);
		enumerator.RunCoroutineWithoutYields();

		return simulatedLevelController;
	}

	private float EvaluateLevelState(LevelController levelController)
	{
		const float ALIVE_BOT_SCORE = 9f;
		const float PER_HEALTH_BOT_SCORE = 0.45f;

		const float ALIVE_PLAYER_SCORE = -10f;
		const float PER_HEALTH_PLAYER_SCORE = -0.5f;

		const float RANDOMNESS = 0.0001f;

		float score = 0f;
		foreach (var actor in levelController.ActorController.Actors)
		{
			var unit = actor as Unit;
			if (unit != null)
			{
				if (unit.FactionInput.factionId == factionId)
				{
					if (unit.IsAlive) score += ALIVE_BOT_SCORE;
					score += PER_HEALTH_BOT_SCORE * unit.Health;
				}
				else
				{
					if (unit.IsAlive) score += ALIVE_PLAYER_SCORE;
					score += PER_HEALTH_PLAYER_SCORE * unit.Health;
				}
			}

			var obstacle = actor as Obstacle;
			if (obstacle != null)
			{
				if (obstacle.factionId == factionId)
				{
					if (obstacle.IsAlive) score += ALIVE_BOT_SCORE / 10f;
					score += PER_HEALTH_BOT_SCORE * obstacle.Health / 10f;
				}
				else
				{
					if (obstacle.IsAlive) score += ALIVE_PLAYER_SCORE / 10f;
					score += PER_HEALTH_PLAYER_SCORE * obstacle.Health / 10f;
				}
			}
		}
		score += UnityEngine.Random.Range(-RANDOMNESS, RANDOMNESS);

		return score;
	}

	private IEnumerable<UnitInput> GetMovementsActs(Unit unit)
	{
		var possibleInputs = new List<UnitInput>();
		if (!unit.HasMoved)
		{
			//Add act for each possible end position
			var possiblePaths = LevelController.GetPossiblePaths(unit);
			foreach (var endPos in possiblePaths.Keys)
			{
				var moveInput = new UnitInput { inputType = InputType.MOVE, targetPositions = new List<Vector2Int> { endPos } };
				possibleInputs.Add(moveInput);
			}
		}

		//Debug.Log("FoundMovements acts: " + ObjectDumper.Dump(possibleInputs, 2));

		return possibleInputs;
	}

	private IEnumerable<UnitInput> GetActionActs(Unit unit)
	{
		var possibleInputs = new List<UnitInput>();
		foreach (var unitAction in unit.UnitActionsDefinitions) //For each action
		{
			foreach (var targetResult in unitAction.GetPossibleTargets(unit.Position)) //For each Target that action has
			{
				var validTargetPositions = LevelController.GetValidTargetPositions(unit.Position, targetResult);
				foreach (var targetPosition in validTargetPositions) //Add an action for each possible target position
				{
					var actionAct = new UnitInput { inputType = InputType.ACTION, targetPositions = new List<Vector2Int> { targetPosition }, unitAction = unitAction };
					possibleInputs.Add(actionAct);
				}

			}
		}
		return possibleInputs;
	}
}