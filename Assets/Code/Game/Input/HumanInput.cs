﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HumanInput : FactionInput
{
	public override IEnumerator GetUnitInput(Unit unit, InputType[] validInputTypes)
	{
		bool awaitingInput = true;
		while (awaitingInput)
		{
			UnitInput unitInput = null;

			if (!unit.HasMoved){
				var possiblePaths = LevelController.GetPossiblePaths(unit);
				var clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition).ToInt();

				if (possiblePaths.ContainsKey(clickPosition)){
					var path = possiblePaths[clickPosition];
					EventManager.OnShowPath(path);
				}else{
					EventManager.OnShowPath(new Path());
				}
			}

			if (Input.GetKeyDown(KeyCode.Tab)){
				EventManager.OnShowRoundOrder(true, LevelController);
			}else if (Input.GetKeyUp(KeyCode.Tab))
			{
				EventManager.OnShowRoundOrder(false, LevelController);
			}

			if (Input.GetMouseButtonDown(0))
			{
				unitInput = GetMovementInput(unit);
			}
			else if (Input.GetKeyDown(KeyCode.Z))
			{
				yield return GetActionInput(unit, 0, (input) => unitInput = input);
			}
			else if (Input.GetKeyDown(KeyCode.X))
			{
				yield return GetActionInput(unit, 1, (input) => unitInput = input);
			}
			else if (Input.GetKeyDown(KeyCode.E))
			{
				unitInput = new UnitInput { inputType = InputType.END_TURN };
			}

			if (unitInput != null && unitInput.inputType != InputType.CANCEL)
			{
				awaitingInput = false;
				yield return unitInput;
			}


			yield return null;
		}
	}

	private IEnumerator GetActionInput(Unit unit, int useActiondIdx, Action<UnitInput> callback)
	{
		if (useActiondIdx >= unit.Data.actionIds.Length) yield break;

		var action = unit.PrepareAction(useActiondIdx);
		UnitInput unitInput = null;
		yield return WaitForActionTarget(unit, action, (input) => unitInput = input);
		unit.HideActionTargets();

		callback(unitInput);
	}

	private IEnumerator WaitForActionTarget(Unit unit, UnitActionDefinition action, Action<UnitInput> callback)
	{
		var unitInput = new UnitInput { inputType = InputType.ACTION, unitAction = action, targetPositions = new List<Vector2Int>() };

		foreach (var targetResult in action.GetPossibleTargets(unit.Position))
		{
			var validTargetPositions = LevelController.GetValidTargetPositions(unit.Position, targetResult);

			EventManager.OnShowTargetPositions(validTargetPositions);

			bool awaitingInput = true;
			while (awaitingInput)
			{
				var actionTargetWork = new Work();
				yield return actionTargetWork.Run(GetActionTarget());

				if (actionTargetWork.result is Vector2Int){
					var inputPosition = (Vector2Int)actionTargetWork.result;
					if (validTargetPositions.Contains(inputPosition))
					{
						unitInput.targetPositions.Add(inputPosition);
						awaitingInput = false;
					}
				}else{
					unitInput.inputType = InputType.CANCEL;
					awaitingInput = false;
				}
			}
		}

		EventManager.OnShowTargetPositions(new List<Vector2Int>());
		callback(unitInput);
	}

	private UnitInput GetMovementInput(Unit unit)
	{
		if (!unit.HasMoved)
		{
			var possiblePaths = LevelController.GetPossiblePaths(unit);
			var clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition).ToInt();

			if (possiblePaths.ContainsKey(clickPosition))
			{
				return new UnitInput { inputType = InputType.MOVE, targetPositions = new List<Vector2Int> { clickPosition } };
			}
		}

		return null;
	}

	public IEnumerator GetActionTarget()
	{
		bool awaitingInput = true;
		while (awaitingInput)
		{
			if (Input.GetMouseButtonDown(0))
			{
				var clickPosition = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
				yield return clickPosition.ToInt();
				awaitingInput = false;
			}else if (Input.GetKeyDown(KeyCode.Escape)){
				awaitingInput = false;
			}else{
				yield return null;
			}
		}
	}
}
