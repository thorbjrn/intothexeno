﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FactionInput
{
	public int factionId;

	public LevelController LevelController { get; set; }

	public abstract IEnumerator GetUnitInput(Unit unit, InputType[] validInputTypes);

}
