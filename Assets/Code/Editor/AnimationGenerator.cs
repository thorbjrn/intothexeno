﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

public static class AnimationGenerator
{

	const string textureFolder = "/Art/last-guardian-sprites";
	const string animClipFolder = "Assets/Animation/Clips";
	const string animControllerFolder = "Assets/Animation/";

	static readonly Dictionary<string, string> lastGuardianKeys = new Dictionary<string, string>{
		{ "fr", "MovingDown"},
		{ "lf", "MovingLeft"},
		{ "rt", "MovingRight"},
		{ "bk", "MovingUp"},

	};

	[MenuItem("IntoTheXeno/Generate Animations")]
	public static void GenerateAnimations()
	{
		var textureFilePaths = Directory.GetFiles(Application.dataPath + textureFolder, "*.png", SearchOption.AllDirectories);
		var animationSetsDict = GetAnimationSets(textureFilePaths);
		var baseAnimController = AssetDatabase.LoadAssetAtPath<AnimatorController>(animControllerFolder + "BaseUnit.controller");

		try{
			int i = 0;
			foreach (var animationSetEntry in animationSetsDict)
			{
				EditorUtility.DisplayProgressBar("Generating Animations", "", i / (float)animationSetsDict.Count);

				var unitName = animationSetEntry.Key;
				var animationSet = animationSetEntry.Value;
				CreateAnimController(baseAnimController, unitName, animationSet);
			}
		}
		finally{
			EditorUtility.ClearProgressBar();
		}

		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
	}

	private static void CreateAnimController(AnimatorController baseAnimController, string unitName, LastGuardianAnimationSet animationSet)
	{
		var overrideController = new AnimatorOverrideController(baseAnimController);
		var overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>(baseAnimController.animationClips.Length);
		overrideController.GetOverrides(overrides);
		int i = 0;
		foreach (var entry in lastGuardianKeys)
		{
			var key = entry.Key;
			var animName = entry.Value;

			var sprites = animationSet.sprites.Where(x => x.name.Contains(key)).ToList();
			var animClip = CreateAnimClip(unitName + "_" + animName, sprites);

			overrides[i] = new KeyValuePair<AnimationClip, AnimationClip>(baseAnimController.animationClips[i], animClip);
			i++;
		}

		overrideController.ApplyOverrides(overrides);

		AssetDatabase.CreateAsset(overrideController, animControllerFolder + unitName + ".controller");
	}

	private static AnimationClip CreateAnimClip(string clipName, List<Sprite> sprites)
	{
		var animClip = new AnimationClip
		{
			frameRate = 60,   // FPS

		};
		var settings = AnimationUtility.GetAnimationClipSettings(animClip);
		settings.loopTime = true;
		AnimationUtility.SetAnimationClipSettings(animClip, settings);

		EditorCurveBinding spriteBinding = new EditorCurveBinding
		{
			type = typeof(SpriteRenderer),
			path = "",
			propertyName = "m_Sprite"
		};
		var spriteKeyFrames = new ObjectReferenceKeyframe[sprites.Count + 1];
		for (int i = 0; i < sprites.Count + 1; i++)
		{
			int spriteIdx = i == sprites.Count ? (i - 1) : i;
			spriteKeyFrames[i] = new ObjectReferenceKeyframe
			{
				time = i / (float)spriteKeyFrames.Length,
				value = sprites[spriteIdx]
			};
		}
		AnimationUtility.SetObjectReferenceCurve(animClip, spriteBinding, spriteKeyFrames);

		AssetDatabase.CreateAsset(animClip, animClipFolder + "/" + clipName + ".anim");

		return animClip;
	}

	private static Dictionary<string, LastGuardianAnimationSet> GetAnimationSets(string[] textureFilePaths)
	{
		var animationSetsDict = new Dictionary<string, LastGuardianAnimationSet>();
		foreach (var filePath in textureFilePaths)
		{
			string assetPath = "Assets" + filePath.Replace(Application.dataPath, "").Replace('\\', '/');
			var sprite = AssetDatabase.LoadAssetAtPath<Sprite>(assetPath);

			var unitName = sprite.name.Split('_')[0];
			if (!animationSetsDict.ContainsKey(unitName))
			{
				animationSetsDict.Add(unitName, new LastGuardianAnimationSet());
			}

			animationSetsDict[unitName].sprites.Add(sprite);
		}

		return animationSetsDict;
	}

	public class LastGuardianAnimationSet{
		public List<Sprite> sprites = new List<Sprite>();
	}
}
