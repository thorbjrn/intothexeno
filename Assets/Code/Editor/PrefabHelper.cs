﻿using UnityEditor;
using UnityEngine;

namespace IntoTheXeno.Editor
{
	public static class PrefabHelper
	{
		public static Component DuplicatePrefab(Component prefab){
			var path = AssetDatabase.GetAssetPath(prefab);
			var instance = (Component) PrefabUtility.InstantiatePrefab(prefab);

			var newPath = AssetDatabase.GenerateUniqueAssetPath(path);
			var newPrefab = PrefabUtility.SaveAsPrefabAsset(instance.gameObject, newPath);
			Object.DestroyImmediate(instance.gameObject);
			return newPrefab.GetComponent(prefab.GetType());
		}

		internal static void DeletePrefab(Component prefab)
		{
			var path = AssetDatabase.GetAssetPath(prefab);
			AssetDatabase.DeleteAsset(path);
		}
	}
}
