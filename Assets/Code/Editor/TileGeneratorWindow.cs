﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace IntoTheXeno.Editor
{
	public class TileGeneratorWindow : EditorWindow
	{
		[SerializeField] List<Texture2D> textures;
		[SerializeField] List<Sprite> sprites;
		[SerializeField] Tile tileBase;

		[MenuItem("IntoTheXeno/Windows/Tile Generator", priority = 10)]
		public static void Open()
		{
			var window = GetWindow<TileGeneratorWindow>();
			window.Show();
		}

		private void OnGUI()
		{
			var serializedObject = new SerializedObject(this);

			EditorGUILayout.PropertyField(serializedObject.FindProperty("textures"), true);
			EditorGUILayout.PropertyField(serializedObject.FindProperty("sprites"), true);
			EditorGUILayout.PropertyField(serializedObject.FindProperty("tileBase"));


			if (GUILayout.Button("Generate tiles"))
			{
				GenerateTiles();
			}


			serializedObject.ApplyModifiedProperties();
		}

		private void GenerateTiles()
		{
			foreach (var tex in textures)
			{
				var texPath = AssetDatabase.GetAssetPath(tex);

				var texSprites = AssetDatabase.LoadAllAssetsAtPath(texPath).Where(x => x is Sprite);
				foreach (Sprite sprite in texSprites)
				{
					GenerateTile(sprite);
				}
			}

			foreach (Sprite sprite in sprites)
			{
				GenerateTile(sprite);
			}

			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		private void GenerateTile(Sprite sprite)
		{
			var tileBasePath = AssetDatabase.GetAssetPath(tileBase);

			var tilePath = tileBasePath.Split('.')[0] + "_" + sprite.name + ".asset";
			Debug.Log("Creating tile from sprite: " + sprite.name);

			AssetDatabase.CopyAsset(tileBasePath, tilePath);
			var tile = AssetDatabase.LoadAssetAtPath<Tile>(tilePath);
			tile.sprite = sprite;
		}
	}
}