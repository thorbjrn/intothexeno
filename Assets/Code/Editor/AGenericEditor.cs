﻿using UnityEditor;
using UnityEngine;

public abstract class AGenericEditor<T> : Editor where T : Object
{
	protected T Target { get { return target as T; }}

	public override void OnInspectorGUI()
	{
		// Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
		serializedObject.Update();

		DrawInspectorGUI();

		// Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
		serializedObject.ApplyModifiedProperties();
	}

	protected virtual void DrawInspectorGUI(){
		DrawDefaultInspector();
	}
}
