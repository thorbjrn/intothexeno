﻿using UnityEngine;

public static class VectorExtensions
{
    public static Vector2Int ToInt(this Vector2 vector2)
    {
        var vector2Int = new Vector2Int(Mathf.RoundToInt(vector2.x - 0.5f), Mathf.RoundToInt(vector2.y - 0.5f));
        return vector2Int;
    }

    public static Vector2Int ToInt(this Vector3 vector3)
    {
        var vector2Int = new Vector2Int(Mathf.RoundToInt(vector3.x - 0.5f), Mathf.RoundToInt(vector3.y - 0.5f));
        return vector2Int;
    }

	public static Vector2Int Normalized(this Vector2Int vector2Int){
		var normalized = new Vector2Int
		{
			x = Mathf.Clamp(vector2Int.x, -1, 1),
			y = Mathf.Clamp(vector2Int.y, -1, 1)
		};
		return normalized;
	}


	public static Vector2 ToVector2(this Vector2Int vector2Int)
	{
		var vector2 = new Vector2(vector2Int.x + 0.5f, vector2Int.y + 0.5f);
		return vector2;
	}

	public static Vector3 ToVector3(this Vector2Int vector2Int)
	{
		var vector3 = new Vector3(vector2Int.x + 0.5f, vector2Int.y + 0.5f, 0f);
		return vector3;
	}
}