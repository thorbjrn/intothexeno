﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class TilemapExtensions
{
	public static TileBase GetTile(this Tilemap tilemap, Vector2Int pos)
	{
		return tilemap.GetTile(new Vector3Int(pos.x, pos.y, 0));
	}
}
