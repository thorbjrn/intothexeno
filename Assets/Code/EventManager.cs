﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public static class EventManager
{
	public static Action<Unit> OnSelectedUnit = delegate {};

	public static Action<List<Vector2Int>> OnShowMovementPositions = delegate { };
	public static Action<List<Vector2Int>> OnShowTargetPositions = delegate { };
	public static Action<List<UnitActionDefinition>, bool[]> OnShowUnitActions = delegate { };
	public static Action<AActor> OnUpdateActorHealth = delegate { };
	public static Action<AActor> OnActorDead = delegate { };

	public static Action<Path> OnShowPath = delegate { };
	public static Action<bool, LevelController> OnShowRoundOrder = delegate { };
}
