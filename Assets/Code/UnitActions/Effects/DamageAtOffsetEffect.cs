﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DamageAtOffsetEffect : IEffectType
{
	public int damage;
	public Vector2Int offset;

	public EffectResult GetEffectResult(Vector2Int attackerPosition, List<Vector2Int> targetPositions)
	{
		var pos = targetPositions[0] + offset;
		return new EffectResult { damage = damage, position = pos };
	}
}