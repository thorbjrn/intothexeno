﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ConjureEffect : IEffectType
{
	public string conjureId;

	public EffectResult GetEffectResult(Vector2Int attackerPosition, List<Vector2Int> targetPositions)
	{
		return new EffectResult { conjureId = conjureId, position = targetPositions[0] };
	}
}