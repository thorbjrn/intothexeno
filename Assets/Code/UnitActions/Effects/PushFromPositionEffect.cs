﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PushFromPositionEffect : IEffectType
{
	public Vector2Int offset;
	public int distance;

	public EffectResult GetEffectResult(Vector2Int attackerPosition, List<Vector2Int> targetPositions)
	{
		if (offset == Vector2Int.zero){
			Debug.LogError("PushFromPositionEffect should have non-zero offset!");
			return null;
		}
		var pos = targetPositions[0] + offset;
		var dir = (pos - targetPositions[0]).Normalized();
		return new EffectResult { forceMove = dir * distance, position = pos };
	}
}
