﻿using System.Collections.Generic;
using UnityEngine;

public interface IEffectType
{
	EffectResult GetEffectResult(Vector2Int attackerPosition, List<Vector2Int> targetPositions);
}
