﻿using System.Linq;
using UnityEngine;

public class EffectResult
{
	public Vector2Int forceMove;
	public Vector2Int position;
	public int damage;
	public string conjureId;

	public override string ToString()
	{
		return GetType().Name + " - " + string.Join(", ", GetType().GetFields().Select(x => x.Name + "=" + x.GetValue(this)).ToArray());
	}
}
