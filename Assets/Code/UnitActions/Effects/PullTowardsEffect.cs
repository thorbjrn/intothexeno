﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PullTowardsEffect : IEffectType
{
	public int distanceFromAttacker;

	public EffectResult GetEffectResult(Vector2Int attackerPosition, List<Vector2Int> targetPositions)
	{
		var pos = targetPositions[0];
		var dir = (pos - attackerPosition).Normalized();
		var endPos = attackerPosition + dir * distanceFromAttacker;
		return new EffectResult { forceMove = endPos - pos, position = pos };
	}
}
