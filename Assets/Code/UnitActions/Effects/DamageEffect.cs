﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DamageEffect : IEffectType
{
	public int damage;

	public EffectResult GetEffectResult(Vector2Int attackerPosition, List<Vector2Int> targetPositions)
	{
		return new EffectResult { damage = damage, position = targetPositions[0] };
	}
}