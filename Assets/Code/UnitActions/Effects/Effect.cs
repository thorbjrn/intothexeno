﻿using System;
using System.Linq;
using UnityEngine;

[Serializable]
public class Effect
{
	public string serializedType;
	public string serializedFields;

	private IEffectType effectType;

	public IEffectType GetEffectType()
	{
		return SerializeHelper.Deserialize<IEffectType>(serializedType, serializedFields);
	}
}
