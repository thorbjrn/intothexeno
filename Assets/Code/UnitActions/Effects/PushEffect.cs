﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PushEffect : IEffectType
{
	public Vector2Int offset;
	public int distance;

	public EffectResult GetEffectResult(Vector2Int attackerPosition, List<Vector2Int> targetPositions)
	{

		var pos = targetPositions[0] + offset;
		var dir = (pos - attackerPosition).Normalized();
		return new EffectResult { forceMove = dir * distance, position = pos };
	}
}
