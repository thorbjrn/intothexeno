﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TargetResult
{
	public List<Vector2Int> possiblePositions;
	public ITargetType targetType;
}
