﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UnitActionDefinition
{
	public string actionId;

	public Target[] targets;
	public Effect[] effects;

	public IEnumerable<EffectResult> GetEffectResults(Vector2Int position, List<Vector2Int> targetPositions)
	{
		int effectIdx = 0;
		while (effectIdx < effects.Length)
		{
			var effect = effects[effectIdx];
			var effectType = effect.GetEffectType();
			var effectResult = effectType.GetEffectResult(position, targetPositions);
			yield return effectResult;
			effectIdx++;
		}
	}

	internal IEnumerable<TargetResult> GetPossibleTargets(Vector2Int position)
	{
		int targetIdx = 0;
		while (targetIdx < targets.Length)
		{
			var target = targets[targetIdx];
			var targetType = target.GetTargetType();
			var possiblePositions = targetType.GetPossibleTargetPositions(position);
			yield return new TargetResult{possiblePositions = possiblePositions, targetType = targetType};
			targetIdx++;
		}
	}
}
