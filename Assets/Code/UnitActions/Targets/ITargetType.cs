﻿using System.Collections.Generic;
using UnityEngine;

public interface ITargetType
{
	List<Vector2Int> GetPossibleTargetPositions(Vector2Int position);
	bool IsBlockedByUnits { get; }
	bool CanTargetUnits { get; }
	bool CanTargetEmpty { get; }
}
