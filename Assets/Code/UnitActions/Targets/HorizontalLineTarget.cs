﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HorizontalLineTarget : ITargetType
{
	public int range;

	public bool isBlockedByUnits;
	public bool IsBlockedByUnits
	{
		get
		{
			return isBlockedByUnits;
		}
	}

	public bool canTargetUnits;
	public bool CanTargetUnits
	{
		get
		{
			return canTargetUnits;
		}
	}

	public bool canTargetEmpty;
	public bool CanTargetEmpty
	{
		get
		{
			return canTargetEmpty;
		}
	}

	public List<Vector2Int> GetPossibleTargetPositions(Vector2Int position)
	{
		var positions = new List<Vector2Int>();
		for (int x = position.x - range; x <= position.x + range; x++)
		{
			var targetPos = new Vector2Int(x, position.y);
			if (targetPos == position) continue;
			positions.Add(targetPos);
		}
		return positions;
	}
}
