﻿using System;
using System.Linq;
using UnityEngine;

[Serializable]
public class Target
{
	public string serializedType;
	public string serializedFields;

	private ITargetType targetType;

	public ITargetType GetTargetType()
	{
		return SerializeHelper.Deserialize<ITargetType>(serializedType, serializedFields);
	}
}
