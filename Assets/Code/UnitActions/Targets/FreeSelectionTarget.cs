﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FreeSelectionTarget : ITargetType
{
	public bool IsBlockedByUnits
	{
		get { return false; }
	}

	public bool canTargetUnits;
	public bool CanTargetUnits
	{
		get
		{
			return canTargetUnits;
		}
	}

	public bool canTargetEmpty;
	public bool CanTargetEmpty
	{
		get
		{
			return canTargetEmpty;
		}
	}

	public List<Vector2Int> GetPossibleTargetPositions(Vector2Int position)
	{
		return new List<Vector2Int> { position };
	}
}
